//TODO: move following configuration somewhere else
const config = require('config');
const dbName = config.get('dbConn.name');
const dbAutoIndex = config.get('dbConn.autoIndex');
const mongoose = require('mongoose');

const { Budget, Allocation, Expense } = require('moneta.juno');
const { BudgetDto } = require('./dal/budget.schema');

function database() {

    this.connect = () => mongoose.connect(dbName, { useCreateIndex: true, useNewUrlParser: true, autoIndex: dbAutoIndex });
    this.disconnect = () => mongoose.disconnect();
    this.reload = () => Promise.resolve(); //TODO: slowly deprecate this (should clean up error handling as well)

    this.getAll = () => 
        BudgetDto.find({})
        .then((result) => result.map((r) => new Budget(r)));

    this.deleteAll = () =>
        BudgetDto.deleteMany();

    this.createBudget = (budgetData) => 
        Promise.resolve(new Budget(budgetData))
        .then((budget) => new BudgetDto(budget))
        .then((dto) => dto.save())
        .then(() => true);

    // Everything is done through the budget domain object, so only one get?
    this.getBudget = (budgetName) => 
        BudgetDto.findOne({ name: budgetName })
        .then((result) => new Budget(result));

    this.budgetExists = (budgetName) => 
        BudgetDto.findOne({ name: budgetName })
        .then((result) => result ? true : false); 

    this.updateBudget = (budgetName, update) => 
        BudgetDto.findOne({ name: budgetName })
        .then((dto) => {

            if (!dto) return false;

            let budget = new Budget(dto);
            
            let startDateUpdate = update.startDate || budget.startDate;
            let endDateUpdate = update.endDate || budget.endDate;
            budget.setDateRange(startDateUpdate, endDateUpdate);

            if (update.name || update.name === '') budget.name = update.name;
            if (update.amount || update.amount === 0) budget.amount = update.amount;
            //TODO: implement a way to update the allocation list?

            return dto.updateOne(budget);

        })
        .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

    this.deleteBudget = (budgetName) => 
        BudgetDto.deleteOne({ name: budgetName })
        .then((queryObj) => queryObj.n === 1 && queryObj.deletedCount === 1);

    this.createAllocation = (budgetName, allocationData) => 
        this.budgetExists(budgetName)
        .then((exists) => {

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {
                
                let budget = new Budget(dto);

                budget.allocate(new Allocation({
                    name: allocationData.name,
                    amount: allocationData.amount
                }))

                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

            });

        });

    //TODO: implement a get allocation? how would you test it?

    this.allocationExists = (budgetName, allocationName) => 
        BudgetDto.findOne({ name: budgetName, 'allocations.name': allocationName })
        .then((result) => result ? true : false);

    this.updateAllocation = (budgetName, allocationName, update) => 
        this.allocationExists(budgetName, allocationName)
        .then((exists) => {

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {

                let budget = new Budget(dto);
                let allocation = budget.allocations.find((a) => a.name === allocationName);

                if (update.name || update.name === '') allocation.name = update.name;
                if (update.amount || update.amount === 0) allocation.amount = update.amount;
                //TODO: implement a way to update the expense list?

                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

            });    

        });

    this.deleteAllocation = (budgetName, allocationName) => 
        this.allocationExists(budgetName, allocationName)
        .then((exists) => {

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {

                let budget = new Budget(dto);
                budget.unallocate(allocationName); 

                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

            });              

        });      

    this.createExpense = (budgetName, allocationName, expenseData) => 
        this.allocationExists(budgetName, allocationName).then((exists) => {

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {

                let budget = new Budget(dto);
                let allocation = budget.allocations.find((a) => a.name === allocationName);

                allocation.record(new Expense({
                    id: generateExpenseId(allocation),
                    entryDate: expenseData.entryDate,
                    description: expenseData.description,
                    amount: expenseData.amount
                }));

                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

            });

        });

    //TODO: implement a get expense? how would you test it?
    //TODO: refactor to test existance by id (cucumber tests may not have id though)
    this.expenseExists = (budgetName, allocationName, entryDate, description, amount) => //there is no "expense name" field, full description is needed instead
        BudgetDto.findOne({
            name: budgetName, 
            "allocations.name": allocationName, 
            "allocations.expenses.entryDate": entryDate,
            "allocations.expenses.description": description,
            "allocations.expenses.amount": amount
        })
        .then((result) => result ? true : false);

    this.updateExpense = (budgetName, allocationName, expenseId, update) => 
        this.allocationExists(budgetName, allocationName).then((exists) => { 

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {
            
                let budget = new Budget(dto);
                let allocation = budget.allocations.find((a) => a.name === allocationName); 
                let expense = allocation.expenses.find((e) => e.id === expenseId);

                if (!expense) return false; //TODO: update to rely on expenseExists after it relies on id

                if (update.entryDate) expense.entryDate = update.entryDate;
                if (update.description || update.description === '') expense.description = update.description;
                if (update.amount || update.amount === 0) expense.amount = update.amount;

                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1);

            });

        });

    this.deleteExpense = (budgetName, allocationName, expenseId) => 
        this.allocationExists(budgetName, allocationName).then((exists) => { 

            if (!exists) return false;

            return BudgetDto
            .findOne({ name: budgetName })
            .then((dto) => {

                let budget = new Budget(dto);
                let allocation = budget.allocations.find((a) => a.name === allocationName);
                let expense = allocation.expenses.find((e) => e.id === expenseId);

                if (!expense) return false; //TODO: update to rely on expenseExists after it relies on id

                allocation.delete(expenseId); //NOTE: delete does not report in any way success or failure      
                return dto
                .updateOne(budget)
                .then((queryObj) => queryObj.n === 1 && queryObj.nModified === 1); 

            });    

        });
}

//TODO: this should go somewhere else?
generateExpenseId = (allocation) => {
    let ids = allocation.expenses.map((item) => item.id);
    return ids.length ? Math.max(...ids) + 1 : 1;
}

/* TODO: use the following for the above after the upgrade to mongoDb?
    _updateBudget(
        {
            name: budgetName, 
            "allocations.name": allocationName
        },
        {$push: {"allocations.$.expenses": expense}},
        {});
*/

module.exports = database;