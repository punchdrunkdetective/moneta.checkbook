const React = require('react');
const { formatDate } = require('../../utility');

class ExpenseForm extends React.Component {

    constructor(initialProps) {
        super(initialProps);
        let expense = initialProps.expense;
        this.state = {
            valid: false, //will update after component updates
            result: '', 
            initialExpense: null,
            entryDate: '',
            description: '',
            amount: ''
        };
    }

    static getDerivedStateFromProps(newProps, prevState) { 
        //form can rely on the composing component for state, state is set by the props
        if (newProps.expense) {
            var newExpense = {
                entryDate: formatDate(newProps.expense.entryDate),
                description: newProps.expense.description,
                amount: newProps.expense.amount.toString()
            };

            if (!prevState.initialExpense || //expense could be set in the props by composing component's DidMount method, in which initialExpense would originally be null
                newExpense.entryDate !== prevState.initialExpense.entryDate ||
                newExpense.description !== prevState.initialExpense.description ||
                newExpense.amount !== prevState.initialExpense.amount) {

                return {
                    initialExpense: newExpense,
                    entryDate: newExpense.entryDate,
                    description: newExpense.description,
                    amount: newExpense.amount
                };
            }
        }
        return {}; //no state change needed
    }

    componentDidUpdate(_, prevState) { 
        //method is essentially recursive if you set state, following condition provides a base case
        if (this.stateHasChanged(this.state, prevState)) {
            this.setState({valid: this.isValid()});
        }
    }

    setEntryDate(event) { this.setState({entryDate: event.target.value}); }
    setDescription(event) { this.setState({description: event.target.value}); }
    setAmount(event) { this.setState({amount: event.target.value}); }

    hasEntryDate() { return this.state.entryDate && this.state.entryDate.trim().length !== 0; }
    hasDescription() { return true; } //does not matter what the description is
    hasAmount() { return this.state.amount && this.state.amount.trim().length !== 0; }

    isValid() { return this.hasEntryDate() && this.hasDescription() && this.hasAmount(); }
    stateHasChanged(newState, oldState) { 
        return newState.entryDate !== oldState.entryDate ||
               newState.description !== oldState.description || 
               newState.amount !== oldState.amount;
    }

    showSuccess() { this.setState({result: 'success'}); }
    showFailure() { this.setState({result: 'failure'}); }

    onSubmit(e) { this.props.onSubmit(this, e); }

    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <div id="result" style={{display: this.state.result ? "block" : "none"}}>{this.state.result}</div>
                <br />
                <input id="entryDate" type="text" placeholder="entry date" value={this.state.entryDate} onChange={this.setEntryDate.bind(this)} /> <br />
                <input id="description" type="text" placeholder="description" value={this.state.description} onChange={this.setDescription.bind(this)} /> <br />
                <input id="amount" type="text" placeholder="amount" value={this.state.amount} onChange={this.setAmount.bind(this)} /> <br />
                <br />
                <button id="save" disabled={!this.state.valid} onClick={this.onSubmit.bind(this)}>Save</button>
            </div>
        );
    }

}

module.exports = { 
    ExpenseForm
}