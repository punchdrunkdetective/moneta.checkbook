const React = require('react');
const { NavBar } = require('../navbar.co');
const { ExpenseForm } = require('./form.co');
const { logError, getDb } = require('../../utility');

class UpdateExpenseForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { //state is only maintained to pass to the ExpenseForm component
            expense: null
        };
    }

    componentDidMount() {
        let db = getDb();
        db
        .connect()
        .then(() => {

            return db.getBudget(this.props.budgetId).then((budget) => { //TODO: should you implement a get expense method for database object?

                let allocation = budget.allocations.find((a) => a.name === this.props.allocationId); 
                let expense = allocation.expenses.find((e) => e.id === this.props.expenseId); 

                this.setState({
                    expense: { 
                        entryDate: expense.entryDate,
                        description: expense.description,
                        amount: expense.amount
                    }                
                });

            });    

        })
        .catch((err) => logError(err))
        .finally(() => db.disconnect());
    }

    update(form, event) {

        event.preventDefault();
        let db = getDb();

        let budgetName = this.props.budgetId;
        let allocationName = this.props.allocationId;
        let expenseId = this.props.expenseId;

        db
        .connect()
        .then(() => {

            let update = {
                entryDate: new Date(form.state.entryDate),
                description: form.state.description,
                amount: parseFloat(form.state.amount)
            }

            return db
                .updateExpense(budgetName, allocationName, expenseId, update)
                .then((success) => {

                    if (!success) throw new Error("Did not update database.");

                    form.showSuccess();
                    this.setState({
                        expense: {
                            entryDate: update.entryDate,
                            description: update.description,
                            amount: update.amount
                        }                
                    });

                });

        })
        .catch((err) => {
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }

    render() {
        return (
            <div>
                <NavBar backPage="views/allocation/view.html" query={{ budget: this.props.budgetId, allocation: this.props.allocationId }} />
                <ExpenseForm 
                    title="Expense Update"
                    expense={this.state.expense}
                    onSubmit={this.update.bind(this)} />
            </div>
        );
    }

}

module.exports = {
    UpdateExpenseForm
}