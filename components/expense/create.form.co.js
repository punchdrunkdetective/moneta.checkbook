const React = require('react');
const { NavBar } = require('../navbar.co');
const { ExpenseForm } = require('./form.co');
const { logError, getDb } = require('../../utility');

class CreateExpenseForm extends React.Component {

    constructor(props) {
        super(props);
    }

    save(form, event) {

        event.preventDefault();
        let db = getDb();

        let budgetName = this.props.budgetId;
        let allocationName = this.props.allocationId;

        db
        .connect()
        .then(() => {

            let data = {
                entryDate: new Date(form.state.entryDate),
                description: form.state.description,
                amount: parseFloat(form.state.amount)
            }

            return db
                .createExpense(budgetName, allocationName, data)
                .then(() => form.showSuccess());

        })
        .catch((err) => {
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }
    
    render() {
        return (
            <div>
                <NavBar backPage="views/allocation/view.html" query={{ budget: this.props.budgetId, allocation: this.props.allocationId }} />
                <ExpenseForm 
                    title="Create Expense"
                    onSubmit={this.save.bind(this)} />
            </div>
        );
    }

}

module.exports = {
    CreateExpenseForm
}