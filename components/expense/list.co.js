'use strict';

const React = require('react'); // needed post compile
const { formatDate, truncateTime } = require('../../utility');

function byEntryDate(e1, e2) {

    let entryDate1 = truncateTime(e1.entryDate); 
    let entryDate2 = truncateTime(e2.entryDate);

    if (entryDate1 < entryDate2) { return -1; }
    if (entryDate1 > entryDate2) { return 1; }
    return 0;

}

exports.ExpenseList = function (props) {

    const expenses = props.expenses ? [].concat(props.expenses).sort(byEntryDate) : []; //NOTE: using copy from props
    const addHandler = props.addHandler;
    const updateHandler = props.updateHandler;
    const deleteHandler = props.deleteHandler;
    const amountTotal = expenses.length ? expenses.reduce((acc, e) => acc + e.amount, 0.00) : 0.00

    return (
        <div className="expenseList">
            <h1>Expenses</h1> <button onClick={() => addHandler()}>Add</button>
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {expenses.map((expense) => 
                        <tr key={expense.id}>
                            <td>{formatDate(expense.entryDate)}</td>
                            <td>{expense.description}</td>
                            <td>{expense.amount.toFixed(2)}</td>
                            <td><a href="#" onClick={() => updateHandler(expense.id)}>update</a></td>
                            <td><a href="#" onClick={() => deleteHandler(expense.id)}>delete</a></td>
                        </tr>    
                    )}
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>Total</td>
                        <td>{amountTotal.toFixed(2)}</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    );

}