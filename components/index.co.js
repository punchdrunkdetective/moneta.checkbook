const React = require('react');
const ipcRenderer = require('electron').ipcRenderer;     
const { logError, getDb } = require('../utility');
const { BudgetList } = require('./budget/list.co');

class Content extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            budgets: []
        }
    }

    componentDidMount() { 

        let db = getDb();

        db
        .connect()
        .then(() => db.getAll())
        .then((budgets) => {

            this.setState({
                budgets: budgets
            });

        })
        .catch((err) => logError(err))
        .finally(() => db.disconnect());

    }

    addHandler() {
        ipcRenderer.send('loadUrl', 'views/budget/create.html');
    }

    detailsHandler(budgetName) {
        ipcRenderer.send('loadUrl', 'views/budget/view.html', {id: budgetName});
    }

    updateHandler(budgetName) {
        ipcRenderer.send('loadUrl', 'views/budget/update.html', {id: budgetName});
    }

    deleteHandler(budgetName) { //default window module currently prevents multi-clicks
        if(window.confirm("WARNING: deleting is permanant")) {

            let db = getDb();

            db
            .connect()
            .then(() => db.deleteBudget(budgetName))
            .then(() => {
                this.setState(prevState => ({
                    budgets: prevState.budgets.filter((budget) => budget.name !== budgetName)
                }))
            })
            .finally(() => db.disconnect());

        }
    }

    render() {
        return (
            <div>
                <BudgetList 
                    budgets={this.state.budgets} 
                    addHandler={this.addHandler.bind(this)}
                    detailsHandler={this.detailsHandler.bind(this)} 
                    updateHandler={this.updateHandler.bind(this)} 
                    deleteHandler={this.deleteHandler.bind(this)} />               
            </div>
        );
    }

}

module.exports = {
    Content
}