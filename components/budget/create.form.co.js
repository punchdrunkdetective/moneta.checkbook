const React = require('react');
const { logError, getDb } = require('../../utility');
const { NavBar } = require('../navbar.co');
const { BudgetForm } = require('./form.co');
const { Budget } = require('moneta.juno');

class BudgetCreateForm extends React.Component {

    save(form, event) {

        event.preventDefault();
        let db = getDb();

        db
        .connect()
        .then(() => {

            let data = new Budget({
                name: form.state.name,
                startDate: new Date(form.state.startDate),
                endDate: new Date(form.state.endDate),
                amount: parseFloat(form.state.amount)
            });
            
            return db
                .createBudget(data)
                .then(() => form.showSuccess());

        })
        .catch((err) => {
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }

    render() {
        return (
            <div>
                <NavBar backPage="views/index.html" />
                <BudgetForm 
                    title="Create Budget"
                    onSubmit={this.save.bind(this)} />                
            </div>
        );
    }

}

module.exports = { 
    BudgetCreateForm
}