'use strict';

const React = require('react'); // needed post compile
const { formatDate } = require('../../utility');

exports.BudgetDetails = function (props) {

    const budget = props.budget;

    const name = budget ? budget.name : '';
    const startDate = budget ? formatDate(budget.startDate) : '';
    const endDate = budget ? formatDate(budget.endDate) : '';
    const amount = budget ? budget.amount: 0.00;
    const availableFunding = budget ? budget.fundsAvailable : 0.00;

    return (
        <div>
            <h1>Budget Details</h1>
            Name: <span id="name">{name}</span> <br />     
            Start Date: <span id="startDate">{startDate}</span> <br />
            End Date: <span id="endDate">{endDate}</span> <br />
            Amount: <span id="amount">{amount.toFixed(2)}</span> <br />
            <br />
            Available Funding: <span id="availableFunding">{availableFunding.toFixed(2)}</span> <br />
            <br />       
        </div>
    );
}
