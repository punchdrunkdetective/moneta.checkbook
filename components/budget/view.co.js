const React = require('react');
const ipcRenderer = require('electron').ipcRenderer;

const { logError, getDb } = require('../../utility');
const { NavBar } = require('../navbar.co');
const { BudgetDetails } = require('./details.co');
const { AllocationList } = require('../allocation/list.co');

class Content extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            budget: null,
            allocations: null
        }     
    }

    loadBudgetState() {

        const db = getDb();  
     
        return db
            .getBudget(this.props.budgetId)
            .then((budget) => {

                this.setState({
                    budget: budget,
                    allocations: budget.allocations
                });

            }); 

    }

    componentDidMount() {
        const db = getDb();        
        db
        .connect()
        .then(() => this.loadBudgetState())         
        .catch((err) => logError(err))
        .finally(() => db.disconnect());
    }

    allocationAddHandler() {
        ipcRenderer.send('loadUrl', 'views/allocation/create.html', {id: this.state.budget.name});
    }

    allocationDetailsHandler(allocationName) {
        ipcRenderer.send('loadUrl', 'views/allocation/view.html', {budget: this.state.budget.name, allocation: allocationName});
    }

    allocationUpdateHandler(allocationName) {
        ipcRenderer.send('loadUrl', 'views/allocation/update.html', {budget: this.state.budget.name, allocation: allocationName});
    }

    allocationDeleteHandler(allocationName) { //default window module currently prevents multi-clicks
        if(window.confirm("WARNING: deleting is permanant")) {                        

            let db = getDb();

            let budgetName = this.props.budgetId;

            db
            .connect()
            .then(() => db.deleteAllocation(budgetName, allocationName))
            .then(() => this.loadBudgetState())
            .catch((err) => logError(err))
            .finally(() => db.disconnect());

        }
    }

    render() {
        return (
            <div>
                <NavBar backPage="views/index.html" />
                <BudgetDetails budget={this.state.budget} />
                <AllocationList 
                    allocations={this.state.allocations}
                    addHandler={this.allocationAddHandler.bind(this)}
                    detailsHandler={this.allocationDetailsHandler.bind(this)}
                    updateHandler={this.allocationUpdateHandler.bind(this)} 
                    deleteHandler={this.allocationDeleteHandler.bind(this)} />
            </div>
        )
    }
}

module.exports = {
    Content
}