'use strict';

const React = require('react'); // needed post compile
const { formatDate, truncateTime } = require('../../utility'); 

function byDateRange(b1, b2) {
    //sort by start date, then end date

    let startDate1 = truncateTime(b1.startDate); 
    let startDate2 = truncateTime(b2.startDate); 

    if (startDate1 < startDate2) { return 1; }
    if (startDate1 > startDate2) { return -1; }

    let endDate1 = truncateTime(b1.endDate); 
    let endDate2 = truncateTime(b2.endDate); 

    if (endDate1 < endDate2) { return 1; }
    if (endDate1 > endDate2) { return -1; }

    return 0;

}

exports.BudgetList = function (props) {

    const budgets = [].concat(props.budgets).sort(byDateRange); //NOTE: using copy from props
    const addHandler = props.addHandler;
    const detailsHandler = props.detailsHandler;
    const updateHandler = props.updateHandler;
    const deleteHandler = props.deleteHandler;

    return (
        <div>
            <h1 style={{display: "inline"}}>Budget List</h1><button onClick={() => addHandler()}>Add</button>
            <table id="budgetList">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Start Date</th> 
                        <th>End Date</th>
                        <th>Amount</th>
                        <th></th> 
                        <th></th>                         
                    </tr>
                </thead>
                <tbody>
                    {budgets.map((budget) => 
                        <tr key={budget.name}>
                            <td><a href="#" onClick={() => detailsHandler(budget.name)}>{budget.name}</a></td>
                            <td>{formatDate(budget.startDate)}</td>
                            <td>{formatDate(budget.endDate)}</td>
                            <td>{budget.amount.toFixed(2)}</td>
                            <td><a href="#" onClick={() => updateHandler(budget.name)}>update</a></td>
                            <td><a href="#" onClick={() => deleteHandler(budget.name)}>delete</a></td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
}
