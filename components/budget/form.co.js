const React = require('react');
const { formatDate } = require('../../utility');

class BudgetForm extends React.Component {

    constructor(initialProps) {
        super(initialProps);
        this.state = { 
            valid: false,
            result: '', 
            initialBudget: null,
            name: '',
            startDate: '',
            endDate: '', 
            amount: ''
        };
    }

    static getDerivedStateFromProps(newProps, prevState) { //TODO: there is a good chance this is actually current state
        if (newProps.budget) {
            let newBudget = {
                name: newProps.budget.name,
                startDate: formatDate(newProps.budget.startDate),
                endDate: formatDate(newProps.budget.endDate),
                amount: newProps.budget.amount.toString()
            };

            if (!prevState.initialBudget ||
                newBudget.name !== prevState.initialBudget.name ||
                newBudget.startDate !== prevState.initialBudget.startDate ||
                newBudget.endDate !== prevState.initialBudget.endDate ||
                newBudget.amount !== prevState.initialBudget.amount){

                return {
                    initialBudget: newBudget,
                    name: newBudget.name,
                    startDate: newBudget.startDate,
                    endDate: newBudget.endDate, 
                    amount: newBudget.amount
                }
            }
        }
        return {};
    }

    componentDidUpdate(_, prevState) {
        if (prevState.name !== this.state.name ||
            prevState.startDate !== this.state.startDate ||
            prevState.endDate !== this.state.endDate ||
            prevState.amount !== this.state.amount) {

            this.validateForm();
        }
    }

    setName(event) { this.setState({ name: event.target.value }); }

    setStartDate(event) { this.setState({ startDate: event.target.value }); }

    setEndDate(event) { this.setState({ endDate: event.target.value }); }

    setAmount(event) { this.setState({ amount: event.target.value }); }

    validateForm() {
        if (this.hasName() &&
            this.hasStartDate() &&
            this.hasEndDate() &&
            this.hasAmount()) {

            this.setState({ valid: true });
        }
        else {
            this.setState({ valid: false });
        }
    }   

    hasName() { return this.state.name && this.state.name.trim().length !== 0 }

    hasStartDate() { return this.state.startDate && this.state.startDate.trim().length !== 0 }

    hasEndDate() { return this.state.endDate && this.state.endDate.trim().length !== 0 }

    hasAmount() { return this.state.amount && this.state.amount.trim().length !== 0 }

    showSuccess() { this.setState({result: 'success'}); }

    showFailure() { this.setState({result: 'failure'}); }

    onSubmit(e) { this.props.onSubmit(this, e); }

    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <div id="result" style={{display: this.state.result ? "block" : "none"}}>{this.state.result}</div>
                <br />
                <input id="name" type="text" placeholder="budget name" value={this.state.name} onChange={this.setName.bind(this)} /> <br />
                <input id="startDate" type="text" placeholder="start date" value={this.state.startDate} onChange={this.setStartDate.bind(this)} /> <br />
                <input id="endDate" type="text" placeholder="end date" value={this.state.endDate} onChange={this.setEndDate.bind(this)} /> <br />
                <input id="amount" type="text" placeholder="amount" value={this.state.amount} onChange={this.setAmount.bind(this)} /> <br />
                <br />
                <button id="save" disabled={!this.state.valid} onClick={this.onSubmit.bind(this)}>Save</button>
            </div>
        );
    }

}

module.exports = {
    BudgetForm
}