const React = require('react');
const { logError, getDb } = require('../../utility');
const { NavBar } = require('../navbar.co');
const { BudgetForm } = require('./form.co');

class BudgetUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            budget: null
        };
    }

    componentDidMount() { 

        let db = getDb();

        db
        .connect()
        .then(() => {

            return db.getBudget(this.props.id).then((budget) => {    
                //TODO: add error handling    
                this.setState({
                    budget: {
                        name: budget.name,
                        startDate: budget.startDate,
                        endDate: budget.endDate,
                        amount: budget.amount
                    }
                });    
            });   

        })
        .finally(() => db.disconnect());

    }

    update(form, event) {

        event.preventDefault();
        let db = getDb();

        let budgetName = this.state.budget.name;

        db
        .connect()
        .then(() => {

            let update = {
                name: form.state.name,
                startDate: new Date(form.state.startDate),
                endDate: new Date(form.state.endDate),
                amount: parseFloat(form.state.amount)
            }

            return db
                .updateBudget(budgetName, update)
                .then((success) => {

                    if (!success) throw new Error("Did not update database.");

                    form.showSuccess();
                    this.setState({
                        budget: {
                            name: update.name,
                            startDate: update.startDate,
                            endDate: update.endDate,
                            amount: update.amount
                        }
                    });  

                })

        })
        .catch((err) => { 
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }

    render() {
        return (
            <div>
                <NavBar backPage="views/index.html" />
                <BudgetForm 
                    title="Budget Update"
                    budget={this.state.budget}
                    onSubmit={this.update.bind(this)} />
            </div>
        );
    }

}

module.exports = {
    BudgetUpdateForm
}