const React = require('react');
const { NavBar } = require('../navbar.co');
const { AllocationForm } = require('./form.co');
const { logError, getDb } = require('../../utility');

class UpdateAllocationForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { //state is only maintained to pass to the AllocationForm component
            allocation: null
        }
    }

    componentDidMount() { 
        let db = getDb();
        db
        .connect()
        .then(() => {

            return db.getBudget(this.props.budgetId).then((budget) => { //TODO: should you implement a get allocation method for database object?
                let allocation = budget.allocations.find((a) => a.name === this.props.allocationId);
                this.setState({
                    allocation: {
                        name: allocation.name,
                        amount: allocation.amount.toString()
                    }
                });
            })    

        })
        .catch((err) => {
            logError(err);
        })
        .finally(() => db.disconnect());
    }

    update(form, event) {

        event.preventDefault();
        let db = getDb();

        let budgetName = this.props.budgetId;
        let allocationName = this.state.allocation.name;

        db
        .connect()
        .then(() => {

            let update = {
                name: form.state.name,
                amount: parseFloat(form.state.amount)
            };

            return db
                .updateAllocation(budgetName, allocationName, update)
                .then(() => {

                    form.showSuccess();
                    this.setState({
                        allocation: {
                            name: update.name,
                            amount: update.amount.toString()
                        }
                    });

                });

        })
        .catch((err) => {
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }

    render() {
        return (
            <div>
                <NavBar backPage="views/budget/view.html" query={{ id: this.props.budgetId }} />
                <AllocationForm
                    title="Update Allocation"
                    allocation={this.state.allocation}
                    onSubmit={this.update.bind(this)} />
            </div>
        );
    }

}

module.exports = {
    UpdateAllocationForm
}