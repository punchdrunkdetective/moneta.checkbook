const React = require('react');

const { logError, getDb } = require('../../utility');
const { NavBar } = require('../navbar.co');
const { AllocationForm } = require('./form.co');

class CreateAllocationForm extends React.Component {

    constructor(props) {
        super(props);
    }

    save(form, event) {

        event.preventDefault();
        let db = getDb();

        let budgetName = this.props.id;

        db
        .connect()
        .then(() => {

            let data = {
                name: form.state.name,
                amount: parseFloat(form.state.amount)
            };

            return db
                .createAllocation(budgetName, data)
                .then(() => form.showSuccess());

        })
        .catch((err) => {
            logError(err);
            form.showFailure();
        })
        .finally(() => db.disconnect());

    }

    render() {
        return (
            <div>
                <NavBar backPage="views/budget/view.html" query={{ id: this.props.id }}/>
                <AllocationForm
                    title="Create Allocation"
                    onSubmit={this.save.bind(this)}/>                
            </div>
        );
    }

}

module.exports = {
    CreateAllocationForm
}