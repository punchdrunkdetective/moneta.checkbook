'use strict';

const React = require('react'); // needed post compile

function byName(a1, a2) {

    let name1 = a1.name.toUpperCase();
    let name2 = a2.name.toUpperCase();

    if (name1 < name2) { return -1; }
    if (name1 > name2) { return 1; }
    return 0;

}

exports.AllocationList = function (props) {

    const allocations = props.allocations ? [].concat(props.allocations).sort(byName) : []; //NOTE: using copy from props
    const addHandler = props.addHandler;
    const detailsHandler = props.detailsHandler;
    const updateHandler = props.updateHandler;
    const deleteHandler = props.deleteHandler;
    const totalAmount = allocations.length ? allocations.reduce((acc, a) => acc + a.amount, 0.00) : 0.00;
    const totalSpent = allocations.length ? allocations.reduce((acc, a) => acc + a.spent, 0.00) : 0.00;;
    const totalUnspent = allocations.length ? allocations.reduce((acc, a) => acc + a.unspent, 0.00) : 0.00;;

    return (
        <div className="allocationList">
            <h1>Allocations</h1> <button onClick={() => addHandler()}>Add</button>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Spent</th>
                        <th>Difference</th>
                        <th></th> 
                        <th></th> 
                    </tr>
                </thead>
                <tbody>
                    {allocations.map((allocation) => 
                        <tr key={allocation.name}>
                            <td><a href="#" onClick={() => detailsHandler(allocation.name)}>{allocation.name}</a></td>
                            <td>{allocation.amount.toFixed(2)}</td>
                            <td>{allocation.spent.toFixed(2)}</td>
                            <td>{allocation.unspent.toFixed(2)}</td>
                            <td><a href="#" onClick={() => updateHandler(allocation.name)}>update</a></td>
                            <td><a href="#" onClick={() => deleteHandler(allocation.name)}>delete</a></td>
                        </tr>
                    )}
                </tbody>
                <tfoot>
                    <tr>
                        <td>Totals</td>
                        <td>{totalAmount.toFixed(2)}</td>
                        <td>{totalSpent.toFixed(2)}</td>
                        <td>{totalUnspent.toFixed(2)}</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    );

}