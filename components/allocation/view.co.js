const React = require('react');
const ipcRenderer = require('electron').ipcRenderer;
const { NavBar } = require('../navbar.co');
const { AllocationDetails } = require('./details.co');
const { ExpenseList } = require('../expense/list.co'); 
const { logError, getDb } = require('../../utility');

class Content extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            budget: null, 
            allocation: null,
            expenses: null
        }
    }

    loadBudgetState() {
        let db = getDb();
        return db
            .getBudget(this.props.budgetId) //TODO: implement a getAllocation for db object?
            .then((budget) => {

                let allocation = budget.allocations.find((a) => a.name === this.props.allocationId);
                this.setState({
                    budget: budget,
                    allocation: allocation,
                    expenses: allocation.expenses
                });

            }); 
    }

    componentDidMount() {
        let db = getDb();
        db
        .connect()
        .then(() => this.loadBudgetState()) 
        .catch((err) => logError(err))
        .finally(() => db.disconnect());
    }

    expenseAddHandler() {
        ipcRenderer.send('loadUrl', 'views/expense/create.html', {budget: this.props.budgetId, allocation: this.props.allocationId});
    }

    expenseUpdateHandler(expenseId) {
        ipcRenderer.send('loadUrl', 'views/expense/update.html', {budget: this.props.budgetId, allocation: this.props.allocationId, expense: expenseId});
    }

    expenseDeleteHandler(expenseId) {                    
        if(window.confirm("WARNING: deleting is permanant")) {

            let db = getDb();

            let budgetName = this.props.budgetId;
            let allocationName = this.props.allocationId;

            db
            .connect()
            .then(() => db.deleteExpense(budgetName, allocationName, expenseId))
            .then(() => this.loadBudgetState())
            .catch((err) => logError(err))
            .finally(() => db.disconnect());

        }
    }

    render() {
        return(
            <div>
                <NavBar backPage="views/budget/view.html" query={{ id: this.props.budgetId }}/>
                <AllocationDetails allocation={this.state.allocation} />
                <ExpenseList 
                    expenses={this.state.expenses} 
                    addHandler={this.expenseAddHandler.bind(this)}
                    updateHandler={this.expenseUpdateHandler.bind(this)}
                    deleteHandler={this.expenseDeleteHandler.bind(this)} />
            </div>
        )                    
    }

}

module.exports = {
    Content
}