'use strict';

const React = require('react'); // needed post compile

exports.AllocationDetails = function (props) {

    const allocation = props.allocation;

    const name = allocation ? allocation.name : '';
    const amount = allocation ? allocation.amount : 0.00;
    const spent = allocation ? allocation.spent : 0.00;
    const unspent = allocation ? allocation.unspent : 0.00;

    return (
        <div>
            <h1>Allocation Details</h1>
            Name: <span id="name">{name}</span> <br />
            Amount: <span id="amount">{amount.toFixed(2)}</span> <br />
            Spent: <span id="spent">{spent.toFixed(2)}</span> <br />
            Unspent: <span id="unspent">{unspent.toFixed(2)}</span> <br />
        </div>
    );
}