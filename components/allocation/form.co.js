const React = require('react');

class AllocationForm extends React.Component {

    constructor(initialProps) {
        super(initialProps);
        let allocation = initialProps.allocation;
        this.state = {
            valid: false, //will update after component update
            result: '',
            initialAllocation: null,
            name: '',
            amount: ''
        };
    }

    static getDerivedStateFromProps(newProps, prevState) { //TODO: there is a good chance prevState is actuall current state
        //sense this form can rely on the composing component for state, state is set by the props
        if (newProps.allocation) {
            if (!prevState.initialAllocation || //allocation could be set in the props by composing component's DidMount method, in which initialAllocation would originally be null
                newProps.allocation.name !== prevState.initialAllocation.name ||
                newProps.allocation.amount !== prevState.initialAllocation.amount) {

                return {
                    initialAllocation: newProps.allocation,
                    name: newProps.allocation.name,
                    amount: newProps.allocation.amount
                }
            }
        }
        return {}; //no state change needed
    }

    componentDidUpdate(_, prevState) { 
        //method is essentiall recursive if you set state, following condition provides a base case
        if (this.stateHasChanged(this.state, prevState)) { 
            if (this.isValid()) {
                this.setState({ valid: true });
            }
            else {
                this.setState({ valid: false });
            }        
        }
    }

    setName(event) { this.setState({name: event.target.value}); }
    setAmount(event) { this.setState({amount: event.target.value})}

    hasName() { return this.state.name && this.state.name.trim().length !== 0; }
    hasAmount() {return this.state.amount && this.state.amount.trim().length !== 0; }

    isValid() { return this.hasName() && this.hasAmount(); }
    stateHasChanged(newState, oldState) { 
        return newState.name !== oldState.name || 
               newState.amount !== oldState.amount;
    }

    showSuccess() { this.setState({result: 'success'}); }
    showFailure() { this.setState({result: 'failure'}); }

    onSubmit(e) { this.props.onSubmit(this, e); }

    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <div id="result" style={{display: this.state.result ? "block" : "none"}}>{this.state.result}</div>
                <br />
                <input id="name" type="text" placeholder="budget name" value={this.state.name} onChange={this.setName.bind(this)}/> <br />
                <input id="amount" type="text" placeholder="amount" value={this.state.amount} onChange={this.setAmount.bind(this)} /> <br />
                <br />
                <button id="save" disabled={!this.state.valid} onClick={this.onSubmit.bind(this)}>Save</button>
            </div>
        );
    }

}

module.exports = {
    AllocationForm
}