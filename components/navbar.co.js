const React = require('react');
const ipcRenderer = require('electron').ipcRenderer;

class NavBar extends React.Component {

    constructor(props) {
        super(props);
    }

    goHome() {
        ipcRenderer.send('loadUrl', 'views/index.html'); 
    }

    goBack() {
        ipcRenderer.send('loadUrl', this.props.backPage, this.props.query);
    }

    render() {
        return (
            <div>
                <a id="home" href="#" onClick={this.goHome.bind(this)}>Home</a> <br />
                <a id="back" href="#" onClick={this.goBack.bind(this)}>Back</a>         
            </div>
        );
    }
      
}

module.exports = {
    NavBar
}