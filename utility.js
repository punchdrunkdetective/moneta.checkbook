const ipcRenderer = require('electron').ipcRenderer;
const Database = require('./database');

const logError = (err) => ipcRenderer.send('logError', err);
const formatDate = (date) => `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
const truncateTime = (date) => new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0)

let db;
function getDb() {
    if (db == undefined) {
        db = new Database();
    }
    return db;
}

module.exports = {
    logError,
    formatDate,
    truncateTime,
    getDb
}