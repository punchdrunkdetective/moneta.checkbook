@budgetRegression
Feature: View a budget.

    As a financial planner, I want to be able to review budget details from a single view. 

    Background:
        Given An existing budget "test budget" 8/7/2018 9/3/2018 200.00
        And The budget "test budget" has an existing 20.00 allocated for "gas"
        And The budget "test budget" has an existing 130.00 allocated for "grocery"
        And The budget "test budget" has an existing 50.00 allocated for "food"

    Scenario: View an existing budget
        Given I am viewing "test budget"
        Then The budget details for "test budget" should show "test budget" 8/7/2018 9/3/2018 200.00
        And The budget details for "test budget" should show available funding of 0.00
        And The allocation list for "test budget" should show "gas" 20.00 0.00 20.00
        And The allocation list for "test budget" should show "grocery" 130.00 0.00 130.00
        And The allocation list for "test budget" should show "food" 50.00 0.00 50.00


