export interface BudgetData {
    name: string;
    startDate: Date;
    endDate: Date;
    amount: number;
}