export interface ExpenseData {
    entryDate: Date;
    description: string;
    amount: number;
}