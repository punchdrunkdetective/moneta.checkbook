export interface AllocationData {
    name: string;
    amount: number;
    spent?: number;
    unspent?: number;
}