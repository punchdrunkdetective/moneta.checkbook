import { defineParameterType } from 'cucumber';

defineParameterType({
    regexp: /\d{1,2}\/\d{1,2}\/\d{4}/,
    preferForRegexpMatch: true,
    transformer(dateStr: string) {
      return new Date(dateStr);
    },
    name: 'date'
});