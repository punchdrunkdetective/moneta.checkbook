import { BeforeAll, AfterAll, Before, After, setDefaultTimeout, Status } from 'cucumber';
import { getApp, getDb } from '../helpers/app';
import { IndexPage } from '../page_objects/IndexPage';

const app = getApp(); //TODO: factor module away
const db = getDb();

const timeout: number = 5000;

BeforeAll(function () {
    return db.connect();
})

BeforeAll(function () {
    setDefaultTimeout(timeout * 2.5); // setting up timeout for a hooks and steps to finish
    return app.start();
});

BeforeAll(function () {
    app.client.timeouts({ // setting up timeouts for spectron operations
        "script": 1000,
        "pageLoad": 7000,
        "implicit": timeout * 1.5
    });
});

AfterAll(function () {
    if (app.isRunning()) {
        return app.stop();
    }
});

AfterAll(function () {
    return db.disconnect();
})

Before(function () {
    return db.deleteAll();
});

Before(function () {
    const startingPoint = new IndexPage();
    return startingPoint.load(); // start all tests at known page
});

After(function (testcase: any) {    
    let world = this; 
    if (testcase.result.status === Status.FAILED) {
        return getApp().browserWindow.capturePage().then(function (imageBuffer: any) {       
            world.attach(imageBuffer, 'image/png');
        });
    }
});