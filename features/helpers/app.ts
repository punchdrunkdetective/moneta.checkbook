const { Application } = require('spectron')

const electronPath = require('electron');
const path = require('path');

const Database = require('../../database');

let app: any;
let db: any;

export function getApp(): any {
    if (app == undefined) {
        app = new Application({
            path: electronPath,
            args: [path.join(__dirname, '..\\..\\')]
        });
    }
    return app;
}

export function getDb(): any {
    if (db == undefined) {
        db = new Database();
    }
    return db;
}

export function formatDate(date: Date): string {
    return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`
}
