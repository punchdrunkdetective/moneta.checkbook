@expenseRegression
Feature: Delete an Expense 

    As a financial planner, I want to be able to delete an expense that was incorrectly made or not needed.

    #TODO: assert aggregates in the background steps
    Background: 
        Given An existing budget "test budget" of 1000.00
        And The budget "test budget" has an existing 800.00 allocated for "general spending"
        And The allocation "general spending" in "test budget" has expense 1 8/8/2019 "testing" 25.00
        And The allocation "general spending" in "test budget" has expense 2 8/9/2019 "testing" 25.00
        And The allocation "general spending" in "test budget" has expense 3 8/10/2019 "testing" 25.00
        And The allocation "general spending" in "test budget" has expense 4 8/11/2019 "testing" 25.00

    Scenario Outline: Delete From List

        Delete an existing expense from the expense list shown on some alloction details page.

        Given I am viewing "general spending" in "test budget"
        When I delete <entryDate> <description> <amount> from the expense list
        And I confirm I want to delete the expense
        Then The expense list for "general spending" in "test budget" should not show <entryDate> <description> <amount>
        And The expense list for "general spending" in "test budget" should show a total of <spent>
        And The allocation list for "test budget" should show "general spending" 800.00 <spent> <unspent>
        And The datastore should not contain <entryDate> <description> <amount> for "general spending" in "test budget"

        Examples:
            | entryDate | description | amount | spent | unspent |
            | 8/10/2019 | "testing"   | 25.00  | 75.00 | 725.00  |

    Scenario Outline: Cancel Delete From List

        Cancel the deletion of an existing expense from the expense list shown on some allocation detials page.

        Given I am viewing "general spending" in "test budget"
        When I delete <entryDate> <description> <amount> from the expense list
        And I cancel I want to delete the expense
        Then The expense list for "general spending" in "test budget" should show <entryDate> <description> <amount>
        And The expense list for "general spending" in "test budget" should show a total of <spent>
        And The allocation list for "test budget" should show "general spending" 800.00 <spent> <unspent>
        And The datastore should contain <entryDate> <description> <amount> for "general spending" in "test budget"

        Examples:
            | entryDate | description | amount | spent | unspent |
            | 8/10/2019 | "testing"   | 25.00  | 100.00 | 700.00  |