@allocationRegression
Feature: Delete an Allocation.

    As a financial planner, I want to be able to delete an allocation that was incorrectly made or not needed.

    #TODO: assert aggregates in the background steps
    #TODO: remove database asserts once you get integration test suite
    Background:
        Given An existing budget "test budget" of 1200.00
        And The budget "test budget" has an existing 100.00 allocated for "gas"
        And The budget "test budget" has an existing 800.00 allocated for "grocery"
        And The budget "test budget" has an existing 100.00 allocated for "free spending"

    Scenario Outline: Delete From List

        Delete an existing allocation from the allocation list shown in the budget details.

        Given I am viewing "test budget"
        When I delete <name> from the allocation list
        And I confirm I want to delete the allocation
        Then The allocation list for "test budget" should not show <name> <amount> <spent> <unspent>
        And The allocation list for "test budget" should show total amount of <allocated>
        And The budget details for "test budget" should show available funding of <unallocated>
        And The datastore should not contain <name> in "test budget"

        Examples:
            | name            | amount | spent | unspent | allocated | unallocated |
            | "free spending" | 100.00 | 0.00  | 100.00  | 900.00    | 300.00      |

    Scenario Outline: Cancel Delete From List

        Cancel the deletion of an existing allocation from the allocation list shown in the budget details.

        Given I am viewing "test budget"
        When I delete <name> from the allocation list
        And I cancel that I want to delete the allocation
        Then The allocation list for "test budget" should show <name> <amount> <spent> <unspent>
        And The allocation list for "test budget" should show total amount of 1000.00
        And The budget details for "test budget" should show available funding of 200.00
        And The datastore should contain <name> in "test budget"

        Examples:
            | name            | amount | spent | unspent |
            | "free spending" | 100.00 | 0.00  | 100.00  |




