@expenseRegression
Feature: Update an expense within an allocation.

    As a checkbook balancer, I want to update money spent for a specific purpose.

    Background: 
        Given An existing budget "test budget" 8/7/2018 9/3/2018 1000.00
        And The budget "test budget" has an existing 1000.00 allocated for "general spending"
        And The allocation "general spending" in "test budget" has expense 1 8/8/2018 "testing" 20.00
        And The allocation "general spending" in "test budget" has expense 2 8/29/2018 "testing" 30.00
        And The allocation "general spending" in "test budget" has expense 3 9/2/2018 "testing" 40.00
        And The allocation details for "general spending" in "test budget" should show 
            | name    | general spending |
            | amount  | 1000.00          |
            | spent   | 90.00            |
            | unspent | 910.00           |

    Scenario Outline: Update an expense
        Given I am prompted to update expense 2 for "general spending" in "test budget"
        When I edit expense to <entryDate> <description> <amount>
        And I save expense edits
        Then I should see a success message for expense update
        And The expense list for "general spending" in "test budget" should show <entryDate> <description> <amount>
        And The expense list for "general spending" in "test budget" should show a total of <spent>
        And The allocation list for "test budget" should show "general spending" 1000.00 <spent> <unspent>

        Examples:
            | entryDate | description | amount | spent | unspent |
            | 8/7/2018  | "testing"   | 10.00  | 70.00 | 930.00  |
            | 9/1/2018  | "testing"   | 10.00  | 70.00 | 930.00  |
            | 9/3/2018  | "testing"   | 10.00  | 70.00 | 930.00  |
            | 8/29/2018 | "testing"   | 30.00  | 90.00 | 910.00  |
            | 9/1/2018  | " "         | 10.00  | 70.00 | 930.00  |
            | 9/1/2018  | "testing"   | 0.00   | 60.00 | 940.00  |

    Scenario Outline: Fail to update an expense
        Given I am prompted to update expense 2 for "general spending" in "test budget"
        When I edit expense to <entryDate> <description> <amount>
        And I save expense edits
        Then I should see a failure message for expense update
        And The expense list for "general spending" in "test budget" should not show <entryDate> <description> <amount>
        And The expense list for "general spending" in "test budget" should show a total of 90.00
        And The allocation list for "test budget" should show "general spending" 1000.00 90.00 910.00

        Examples:
            | entryDate | description | amount | 
            | 8/6/2018  | "testing"   | 10.00  |
            | 9/4/2018  | "testing"   | 10.00  |
            | 9/1/2018  | "testing"   | -10.00 |

    Scenario: Update an expense multiple times
        Given I am prompted to update expense 2 for "general spending" in "test budget"
        When I edit expense to 9/1/2018 "test 1" 10.00
        And I save expense edits
        Then I should see a success message for expense update
        When I edit expense to 9/1/2018 "test 2" 10.00
        And I save expense edits
        Then I should see a success message for expense update
        And The expense list for "general spending" in "test budget" should show 9/1/2018 "test 2" 10.00
        And The expense list for "general spending" in "test budget" should show a total of 70.00
        And The allocation list for "test budget" should show "general spending" 1000.00 70.00 930.00