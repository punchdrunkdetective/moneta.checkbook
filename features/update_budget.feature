@budgetRegression
Feature: Update a budget

    As a financial planner, I want to be able to edit a budget that was incorrectly made.

    #TODO: should you add any checks for available funding?
    Background:
        Given An existing budget "other budget" 12/1/2019 1/30/2020 400.00
        And An existing budget "test budget" 8/7/2018 9/3/2018 300.00
        And The budget "test budget" has an existing 200.00 allocated for "expenses"
        And The allocation "expenses" in "test budget" has expense 1 8/15/2018 "testing" 10.00

    Scenario Outline: Edit budget with new values
        Given I am prompted to update "test budget"
        When I edit budget to <newName> <newStartDate> <newEndDate> <newAmount>
        And I save budget edits
        Then I should see a success message for budget update
        And The budget list should show <newName> <newStartDate> <newEndDate> <newAmount>
        And The allocation list for <newName> should show "expenses" 200.00 10.00 190.00

        Examples: Success
            | newName         | newStartDate | newEndDate | newAmount |
            | "better budget" | 7/6/2017     | 10/4/2019  | 250.00    |
            | "test budget"   | 8/15/2018    | 8/15/2018  | 300.00    |
            | "test budget"   | 8/7/2018     | 9/3/2018   | 300.00    |

    Scenario Outline: Fail to edit budget
        Given I am prompted to update "test budget"
        When I edit budget to <newName> <newStartDate> <newEndDate> <newAmount>
        And I save budget edits
        Then I should see a failure message for budget update
        And The budget list should not show <newName> <newStartDate> <newEndDate> <newAmount>
        And The allocation list for "test budget" should show "expenses" 200.00 10.00 190.00

        Examples: 
            | newName         | newStartDate | newEndDate | newAmount |
            | "other budget"  | 8/7/2018     | 9/3/2018   | 300.00    |
            | "better budget" | 9/3/2018     | 8/7/2018   | 300.00    |
            | "better budget" | 8/7/2018     | 8/1/2018   | 300.00    |
            | "better budget" | 8/7/2019     | 9/3/2018   | -300.00   |
            | "better budget" | 8/7/2018     | 9/3/2018   | 100.00    | 
            | "better budget" | 8/16/2018    | 9/3/2018   | 300.00    | 
            | "better budget" | 8/7/2018     | 8/14/2018  | 300.00    | 

    Scenario: Update an budget multiple times

        Update an budget multiple times in a row, without navigating away from the page.

        Given I am prompted to update "test budget"
        When I edit budget to "better budget" 7/6/2017 10/4/2019 250.00
        And I save budget edits
        And I see a success message for budget update
        When I edit budget to "even better budget" 6/5/2016 11/5/2020 400.00
        And I save budget edits
        Then I see a success message for budget update
        And The budget list should show "even better budget" 6/5/2016 11/5/2020 400.00
        And The allocation list for "even better budget" should show "expenses" 200.00 10.00 190.00
