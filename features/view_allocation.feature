@allocationRegression
Feature: View an allocation.

    As a financial planner, I want to be able to review allocation details from a single view. 

    Background:
        Given An existing budget "test budget" 8/7/2018 9/3/2018 200.00
        And The budget "test budget" has an existing 150.00 allocated for "expenses"
        And The budget "test budget" has an existing 50.00 allocated for "free spending"

    Scenario: View an existing allocation
        Given I am viewing "expenses" in "test budget"
        Then The allocation details for "expenses" in "test budget" should show 
            | name    | expenses |
            | amount  | 150.00   |
            | spent   | 0.00     |
            | unspent | 150.00   |


