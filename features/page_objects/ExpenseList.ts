import { getApp, formatDate } from '../helpers/app';
import { ExpenseData } from "../data_objects/ExpenseData";

const app = getApp();

export class ExpenseList {

    expenseExists(expense: ExpenseData): Promise<boolean> { return this._waitForExist(expense, false); }
    expenseNotExists(expense: ExpenseData): Promise<boolean> { return this._waitForExist(expense, true); }

    addExpense(): Promise<void> {
        const addSelector = './/*[text()="Add"]';
        return app.client
            .waitForVisible(addSelector)
            .then(() => app.client.click(addSelector));
    }

    //NOTE: delete by expense data instead of id because id not known from ui (id is internal)
    updateExpense(expense: ExpenseData): Promise<void> {
        return this
            ._waitForVisible(expense)
            .then(() => this._element(expense).click('.//*[text()="update"]'));
    }

    //NOTE: delete by expense data instead of id because id not known from ui (id is internal)
    deleteExpense(expense: ExpenseData): Promise<void> {
        return this
            ._waitForVisible(expense)
            .then(() => this._element(expense).click('.//*[text()="delete"]'));
    }

    confirmDeletion(): Promise<void> { return app.client.alertAccept(); }
    cancelDeletion(): Promise<void> { return app.client.alertDismiss(); }

    getExpenseTotal(): Promise<number> { return app.client.getText('//tfoot//td[3]').then((total: string) => parseFloat(total)); }

    private _waitForExist(expense: ExpenseData, reverse: boolean): Promise<boolean> {        
        return app.client
            .waitForExist(this._rowSelector(expense), 5000, reverse)
            .then(() => true)
            .catch(() => false);
    }

    private _element(expense: ExpenseData): any { return app.client.element(this._rowSelector(expense)); }
    private _waitForVisible(expense: ExpenseData): Promise<void> { return app.client.waitForVisible(this._rowSelector(expense)); }

    private _rowSelector(expense: ExpenseData): string {
        let entryDate = formatDate(expense.entryDate);
        let description = expense.description;
        let amount = expense.amount;
        return `//*[contains(@class, 'expenseList')]//table//tr[td[text()="${entryDate}"] and td[.="${description}"] and td[text()="${amount.toFixed(2).toString()}"]]`; //NOTE: endtryDate and amount will always have a text node
    };

}
