import { getApp } from '../helpers/app';
import { Page } from './Page';
import { ExpenseFrom } from './ExpenseForm';
import { ExpenseData } from "../data_objects/ExpenseData";

const app = getApp();

export class UpdateExpensePage extends Page { 

    private _pageIndicator = '//h1[text()="Expense Update"]';
    private _amountSelector = '//*[@id="amount"]'; //TODO: perhaps ExpenseForm needs a isLoaded method

    private _form = new ExpenseFrom();    

    load(budgetName: string, allocationName: string, expense: number): Promise<void> {
        return super
            .load('views/expense/update.html', {budget: budgetName, allocation: allocationName, expense: expense}, this._pageIndicator)
            .then(() =>
                app.client.waitUntil( // default values are loaded async
                    () => app.client.getValue(this._amountSelector).then((val: string) => val !== ""),
                    5000,
                    "default input never loaded"));
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getExpense() { return this._form.getExpense(); }
    enterExpense(expense: ExpenseData) { return this._form.setExpense(expense); }

    canSave() { return this._form.canSave(); }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }

    getEntryDate() { return this._form.getEntryDate(); }
    enterEntryDate(entryDate: Date) { return this._form.setEntryDate(entryDate); }  
    clearEntryDate() { return this._form.clearEntryDate(); }

    getDescription() { return this._form.getDescription(); }
    enterDescription(description: string) { return this._form.setDescription(description); }  
    clearDescription() { return this._form.clearDescription(); }

    getAmount() { return this._form.getAmount(); }
    enterAmount(amount: number) { return this._form.setAmount(amount); }
    clearAmount() { return this._form.clearAmount(); }

}
