import { getApp } from '../helpers/app';
import { Page } from "./Page";
import { BudgetForm } from "./BudgetForm";
import { BudgetData } from "../data_objects/BudgetData";

const app = getApp();

export class UpdateBudgetPage extends Page {

    private _pageIndicator = '//h1[text()="Budget Update"]';
    private _form = new BudgetForm();

    load(id: string): Promise<void> {    
        return super
            .load('views/budget/update.html', {id: id}, this._pageIndicator)
            .then(() =>
                app.client.waitUntil( // default values are loaded async
                    () => this.getName().then((val: string) => val !== ""),
                    5000,
                    "default input never loaded"));
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getName() { return this._form.getName(); }
    enterName(name: string) { return this._form.setName(name); }
    clearName() { return this._form.clearName(); }

    getStartDate() { return this._form.getStartDate(); }
    enterStartDate(startDate: Date) { return this._form.setStartDate(startDate); }
    clearStartDate() { return this._form.clearStartDate(); }

    getEndDate() { return this._form.getEndDate(); }
    enterEndDate(endDate: Date) { return this._form.setEndDate(endDate); }
    clearEndDate() { return this._form.clearEndDate(); }

    getAmount() { return this._form.getAmount(); }
    enterAmount(amount: number) { return this._form.setAmount(amount); }
    clearAmount() { return this._form.clearAmount(); }

    getBudget() { return this._form.getBudget(); }
    enterBudget(budget: BudgetData) { return this._form.enterBudget(budget); }

    canSave() { return this._form.canSave() }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }

}
