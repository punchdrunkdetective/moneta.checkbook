import { getApp } from '../helpers/app';

const app = getApp();

export class Page {

    //TODO: load paramaters are a carry over from when this use to be a js module. refactor to be class level params
    protected load(pageUrl: any, query: any, pageIndicator: any): Promise<void> { 
        return app.electron.ipcRenderer
            .send('loadUrl', pageUrl, query)
            .then(() => app.client.waitForVisible(pageIndicator)); 
    }

    //TODO: load paramaters are a carry over from when this use to be a js module. refactor to be class level params
    protected isLoaded(pageIndicator: any): Promise<boolean> { 
        return app.client
            .waitForExist(pageIndicator, 500) // speed up wait for quick yes / no answer
            .then(() => true)
            .catch(() => false);
    }

    //TODO: currently assuming all pages will have some form of navigation (this is not actually true, should it be?)
    protected goHome(): Promise<void> {
        return app.client.click('//*[@id="home"]');
    }

    //TODO: currently assuming all pages will have some form of navigation (this is not actually true, should it be?)
    protected goBack(): Promise<void> {
        return app.client.click('//*[@id="back"]');
    }

    protected waitForAlert(): Promise<void> {
        return app.client.waitUntil(
            () => app.client.alertText().then(
                () => true,
                () => false
            ),
            5000,
            "alert never showed"
        );
    }

}
