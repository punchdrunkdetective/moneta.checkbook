import { Page } from "./Page";
import { BudgetList } from "./BudgetList";
import { BudgetData } from "../data_objects/BudgetData";

export class IndexPage extends Page {

    private _pageIndicator = '//h1[text()="Budget List"]';

    load(): Promise<void> { return super.load('views/index.html', {}, this._pageIndicator); }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    private _budgetList = new BudgetList();
    budgetExists(budget: BudgetData): Promise<boolean> { return this._budgetList.budgetExists(budget); }
    budgetNotExists(budget: BudgetData): Promise<boolean> { return this._budgetList.budgetNotExists(budget); }
    addBudget(): Promise<void> { return this._budgetList.addBudget(); }
    viewBudget(budgetName: string): Promise<void> { return this._budgetList.viewBudget(budgetName); }
    updateBudget(budgetName: string): Promise<void> { return this._budgetList.updateBudget(budgetName); }
    deleteBudget(budgetName: string): Promise<void> { return this._budgetList.deleteBudget(budgetName); }
    confirmDeletion(): Promise<void> { return this._budgetList.confirmDeletion(); }

}
