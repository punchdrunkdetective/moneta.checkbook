import { getApp, formatDate } from '../helpers/app';
import { BudgetData } from "../data_objects/BudgetData";

const app = getApp();

export class BudgetList {

    budgetExists(budget: BudgetData): Promise<boolean> { return this._waitForExist(budget, false); }
    budgetNotExists(budget: BudgetData): Promise<boolean> { return this._waitForExist(budget, true); }

    addBudget(): Promise<void> { 
        const addSelector = './/*[text()="Add"]';
        return app.client
            .waitForVisible(addSelector)
            .then(() => app.client.click(addSelector)); 
     }

    viewBudget(budgetName: string): Promise<void> { 
        let selector = this._getRowByNameSelector(budgetName);
        return app.client
            .waitForVisible(selector)
            .then(() => app.client.element(selector).click(`.//a[text()="${budgetName}"]`));
    }

    updateBudget(budgetName: string): Promise<void> { 
        let selector = this._getRowByNameSelector(budgetName);
        return app.client
            .waitForVisible(selector)
            .then(() => app.client.element(selector).click('.//a[text()="update"]'));
    }

    deleteBudget(budgetName: string): Promise<void> { //TODO: maybe make sure this is the delete popup module?
        let selector = this._getRowByNameSelector(budgetName);
        return app.client
            .waitForVisible(selector)
            .then(() => app.client.element(selector).click('.//*[text()="delete"]'));
    }

    confirmDeletion(): Promise<void> { return app.client.alertAccept(); }

    private _getRowSelector(budget: BudgetData): string { return `//*[@id="budgetList"]//*[*[*[text()="${budget.name}"]] and *[text()="${formatDate(budget.startDate)}"] and *[text()="${formatDate(budget.endDate)}"] and *[text()="${budget.amount.toFixed(2).toString()}"]]`; }
    private _getRowByNameSelector(budgetName: string): string { return `//*[@id="budgetList"]//*[*[*[text()="${budgetName}"]]]`; }

    private _waitForExist(budget: BudgetData, reverse: boolean): Promise<boolean> {
        return app.client.waitForExist(this._getRowSelector(budget), 5000, reverse)
            .then(() => true) //NOTE: this "hides" the error message indicating selector not found
            .catch(() => false);
    }

}