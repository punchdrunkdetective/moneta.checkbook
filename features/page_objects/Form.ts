import { getApp } from '../helpers/app';

const app = getApp();

export class Form {

    private _saveSelector = '//*[@id="save"]';
    private _msgSelector = '//*[@id="result"]';

    canSave() { return app.client.element(this._saveSelector).isEnabled(); }
    save() { return app.client.click(this._saveSelector); }

    getMsg() { 
        return app.client.waitForVisible(this._msgSelector) //TODO: is an explicit wait really neccarry? 
            .then(() => app.client.element(this._msgSelector)) 
            .then(() => app.client.getText(this._msgSelector));
    }

}