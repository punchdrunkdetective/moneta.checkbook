import { Page } from "./Page";
import { ExpenseFrom } from "./ExpenseForm";
import { ExpenseData } from "../data_objects/ExpenseData";

export class CreateExpensePage extends Page {

    private _pageIndicator = '//h1[text()="Create Expense"]'; 
    private _form = new ExpenseFrom();

    load(budgetName: string, allocationName: string): Promise<void> {
        return super.load('views/expense/create.html', {budget: budgetName, allocation: allocationName}, this._pageIndicator);
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getExpense() { return this._form.getExpense(); }
    enterExpense(expense: ExpenseData) { return this._form.setExpense(expense); }

    canSave() { return this._form.canSave(); }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }

    getEntryDate() { return this._form.getEntryDate(); }
    enterEntryDate(entryDate: Date) { return this._form.setEntryDate(entryDate); }
    clearEntryDate() { return this._form.clearEntryDate(); }

    getDescription() { return this._form.getDescription(); }
    enterDescription(description: string) { return this._form.setDescription(description); }
    clearDescription() { return this._form.clearDescription(); }

    getAmount() { return this._form.getAmount(); }
    enterAmount(amount: number) { return this._form.setAmount(amount); }
    clearAmount() { return this._form.clearAmount(); }

}
