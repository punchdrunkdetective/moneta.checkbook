import { getApp, formatDate } from '../helpers/app';
import { Form } from "./Form";
import { BudgetData } from "../data_objects/BudgetData";

const app = getApp();

export class BudgetForm extends Form {
    
    private _nameSelector = '//*[@id="name"]';
    private _startDateSelector = '//*[@id="startDate"]';
    private _endDateSelector = '//*[@id="endDate"]';
    private _amountSelector = '//*[@id="amount"]';

    getBudget(): Promise<BudgetData> {    
        return Promise.all([
            this.getName(),
            this.getStartDate(),
            this.getEndDate(),
            this.getAmount()
        ])
        .then((results) => {
            return {
                name: results[0],
                startDate: results[1],
                endDate: results[2],
                amount: results[3]
            };
        });
    }
    
    enterBudget(budget: BudgetData): Promise<void> {    
        return this.setName(budget.name) //NOTE: react render may be forcing chained promises instead of Promise.all
            .then(() => this.setStartDate(budget.startDate))
            .then(() => this.setEndDate(budget.endDate))
            .then(() => this.setAmount(budget.amount));
    }

    clearName(): Promise<void> { return app.client.setValue(this._nameSelector, ' '); } //NOTE: react may not accept ''
    getName(): Promise<string> { return app.client.getValue(this._nameSelector); }
    setName(name: string): Promise<void> { return this.clearName().then(() => app.client.setValue(this._nameSelector, name)); }

    clearStartDate(): Promise<void> { return app.client.setValue(this._startDateSelector, ' '); } //NOTE: react may not accept ''
    getStartDate(): Promise<Date> { return app.client.getValue(this._startDateSelector).then((dateStr: string) => dateStr ? new Date(dateStr) : ''); }
    setStartDate(startDate: Date): Promise<void> { return this.clearStartDate().then(() => app.client.setValue(this._startDateSelector, formatDate(startDate))); }

    clearEndDate(): Promise<void> { return app.client.setValue(this._endDateSelector, ' '); } //NOTE: react may not accept ''
    getEndDate(): Promise<Date> { return app.client.getValue(this._endDateSelector).then((dateStr: string) => dateStr ? new Date(dateStr) : ''); }
    setEndDate(endDate: Date): Promise<void> { return this.clearEndDate().then(() => app.client.setValue(this._endDateSelector, formatDate(endDate))); }

    clearAmount(): Promise<void> { return app.client.setValue(this._amountSelector, ' '); } //NOTE: react may not accept ''
    getAmount(): Promise<number> { return app.client.getValue(this._amountSelector).then((amount: string) => amount ? parseFloat(amount) : 0.00); }
    setAmount(amount: number): Promise<void> { return this.clearAmount().then(() => app.client.setValue(this._amountSelector, amount)); }

}