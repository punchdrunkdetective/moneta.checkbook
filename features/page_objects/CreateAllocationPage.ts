import { Page } from "./Page";
import { AllocationForm } from "./AllocationForm";
import { AllocationData } from "../data_objects/AllocationData";

export class CreateAllocationPage extends Page{

    private _pageIndicator = '//h1[text()="Create Allocation"]';
    private _form = new AllocationForm();

    load(id: string): Promise<void> { return super.load('views/allocation/create.html', {id: id}, this._pageIndicator); }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getName() { return this._form.getName(); }
    enterName(name: string) { return this._form.setName(name); }

    getAmount() { return this._form.getAmount(); }
    enterAmount(amount: number) { return this._form.setAmount(amount); }

    enterAllocation(allocation: AllocationData) { return this._form.setAllocation(allocation); } 

    canSave() { return this._form.canSave(); }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }
}
