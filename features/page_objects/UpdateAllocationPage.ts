import { getApp } from '../helpers/app';
import { Page } from "./Page";
import { AllocationForm } from "./AllocationForm";
import { AllocationData } from "../data_objects/AllocationData";

const app = getApp();

export class UpdateAllocationPage extends Page {

    private _pageIndicator = '//h1[text()="Update Allocation"]';
    private _form = new AllocationForm();

    load(budgetName: string, allocationName: string): Promise<void> {        
        return super
            .load('views/allocation/update.html', {budget: budgetName, allocation: allocationName}, this._pageIndicator)
            .then(() =>
                app.client.waitUntil( // default values are loaded async
                    () => this.getName().then((val: string) => val !== ""),
                    5000,
                    "default input never loaded"));
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getName() { return this._form.getName(); }
    setName(allocationName: string) { return this._form.setName(allocationName); }
    clearName() { return this._form.clearName(); }

    getAmount() { return this._form.getAmount(); }
    setAmount(amount: number) { return this._form.setAmount(amount); }
    clearAmount() { return this._form.clearAmount(); }

    getAllocation() { return this._form.getAllocation(); }
    enterAllocation(allocation: AllocationData) { return this._form.setAllocation(allocation); } 

    canSave() { return this._form.canSave(); }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }

}
