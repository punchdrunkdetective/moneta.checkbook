import { getApp } from '../helpers/app';
import { Page } from './Page';
import { AllocationList } from './AllocationList';
import { BudgetData } from "../data_objects/BudgetData";
import { AllocationData } from "../data_objects/AllocationData";

const app = getApp();

export class ViewBudgetPage extends Page {

    //TODO: perhaps the load method should implicitly check if the page is loaded or not first? (what do you do for forced refresh though?)
    private _pageIndicator = '//h1[text()="Budget Details"]';

    load(id: string): Promise<void> {        
        return super
            .load('views/budget/view.html', {id: id}, this._pageIndicator)
            .then(() =>
                app.client.waitUntil( // default values are loaded async
                    () => this.getName().then((val: string) => val !== ""),
                    5000,
                    "default input never loaded"));
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getName(): Promise<string> { return app.client.getText('//*[@id="name"]'); }
    getStartDate(): Promise<Date> { return app.client.getText('//*[@id="startDate"]').then((dateStr: string) => new Date(dateStr)); }
    getEndDate(): Promise<Date> { return app.client.getText('//*[@id="endDate"]').then((dateStr: string) => new Date(dateStr)); } 
    getAmount(): Promise<number> { return app.client.getText('//*[@id="amount"]').then((amount: string) => parseFloat(amount)); }

    getBudgeDetails(): Promise<BudgetData> {        
        return Promise.all([
            this.getName(),
            this.getStartDate(),
            this.getEndDate(),
            this.getAmount()
        ])
        .then((results: any[]) => {
            return {
                name: results[0],
                startDate: results[1],
                endDate: results[2],
                amount: results[3]
            };
        });
    }

    getAvailableFunding(): Promise<number> { return app.client.getText('//*[@id="availableFunding"]').then((funding: string) => parseFloat(funding)); }

    private _allocationList = new AllocationList();
    allocationExists(allocation: AllocationData) { return this._allocationList.allocationExists(allocation); }
    allocationNotExists(allocation: AllocationData) { return this._allocationList.allocationNotExists(allocation); }
    getAllocation(allocationName: string) { return this._allocationList.getAllocation(allocationName); }
    addAllocation() { return this._allocationList.addAllocation(); }
    viewAllocation(allocationName: string) { return this._allocationList.viewAllocation(allocationName); }
    updateAllocation(allocationName: string) { return this._allocationList.updateAllocation(allocationName); }
    deleteAllocation(allocationName: string) { return this._allocationList.deleteAllocation(allocationName); }
    confirmDeletion() { return this._allocationList.confirmDeletion(); }
    cancelDeletion() { return this._allocationList.cancelDeletion(); }
    getTotalAllocated() { return this._allocationList.getTotalAmount(); }

}
