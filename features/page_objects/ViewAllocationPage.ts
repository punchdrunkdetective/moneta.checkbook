import { getApp } from '../helpers/app';
import { Page } from './Page';
import { ExpenseList } from './ExpenseList';
import { AllocationData } from "../data_objects/AllocationData";
import { ExpenseData } from "../data_objects/ExpenseData";

const app = getApp();

export class ViewAllocationPage extends Page {

    private _pageIndicator = '//h1[text()="Allocation Details"]';

    load(budgetName: string, allocationName: string): Promise<void> {        
        return super
            .load('views/allocation/view.html', {budget: budgetName, allocation: allocationName}, this._pageIndicator)
            .then(() =>
                app.client.waitUntil( // default values are loaded async
                    () => this.getName().then((val: string) => val !== ""),
                    5000,
                    "default input never loaded"));
    }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    getName(): Promise<string> { return app.client.getText('//*[@id="name"]'); }
    getAmount(): Promise<number> { return app.client.getText('//*[@id="amount"]').then((amount: string) => parseFloat(amount)); }
    getSpent(): Promise<number> { return app.client.getText('//*[@id="spent"]').then((spent: string) => parseFloat(spent)); }
    getUnspent(): Promise<number> { return app.client.getText('//*[@id="unspent"]').then((unspent: string) => parseFloat(unspent)); }

    getAllocation(): Promise<AllocationData> {
        return Promise.all([
            this.getName(),
            this.getAmount(),
            this.getSpent(),
            this.getUnspent()
        ])
        .then((results: any[]) => {
            return {
                name: results[0],
                amount: parseFloat(results[1]),
                spent: parseFloat(results[2]),
                unspent: parseFloat(results[3])
            }
        });
    }

    private _expenseList = new ExpenseList();
    expenseExists(expense: ExpenseData) { return this._expenseList.expenseExists(expense); }
    expenseNotExists(expense: ExpenseData) { return this._expenseList.expenseNotExists(expense); }
    addExpense() { return this._expenseList.addExpense(); }
    updateExpense(expense: ExpenseData) { return this._expenseList.updateExpense(expense); }
    deleteExpense(expense: ExpenseData) { return this._expenseList.deleteExpense(expense); }
    confirmDeletion() { return this._expenseList.confirmDeletion(); }
    cancelDeletion() { return this._expenseList.cancelDeletion(); }
    getExpenseTotal() { return this._expenseList.getExpenseTotal(); }

}
