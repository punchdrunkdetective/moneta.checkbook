import { getApp } from '../helpers/app';
import { Form } from "./Form";
import { AllocationData } from "../data_objects/AllocationData";

const app = getApp();

export class AllocationForm extends Form {

    private _nameSelector = '//*[@id="name"]';
    private _amountSelector = '//*[@id="amount"]';
    
    getAllocation(): Promise<AllocationData> {
        return Promise.all([
            this.getName(),
            this.getAmount()
        ])
        .then((results) => {
            return {
                name: results[0],
                amount: results[1]
            }
        });
    }

    setAllocation(allocation: AllocationData): Promise<void> {
        return this
            .setName(allocation.name)
            .then(() => this.setAmount(allocation.amount));

    }
        
    clearName(): Promise<void> { return app.client.setValue(this._nameSelector, ' '); } //NOTE: react may not trigger change event with ''
    getName(): Promise<string> { return app.client.getValue(this._nameSelector); }
    setName(allocationName: string): Promise<void> { return app.client.setValue(this._nameSelector, allocationName); }
        
    clearAmount(): Promise<void> { return app.client.setValue(this._amountSelector, ' '); } //NOTE: react may not trigger change event with ''
    getAmount(): Promise<number> { return app.client.getValue(this._amountSelector).then((amtStr: string) => amtStr ? parseFloat(amtStr) : 0.00); }
    setAmount(amount: number): Promise<void> { return app.client.setValue(this._amountSelector, amount); }
    
}