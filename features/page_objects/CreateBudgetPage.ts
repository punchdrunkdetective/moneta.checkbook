import { Page } from "./Page";
import { BudgetForm } from "./BudgetForm";
import { BudgetData } from "../data_objects/BudgetData";

export class CreateBudgetPage extends Page {

    private _pageIndicator = '//h1[text()="Create Budget"]';
    private _form = new BudgetForm();

    load(): Promise<void> { return super.load('views/budget/create.html', {}, this._pageIndicator); }
    isLoaded(): Promise<boolean> { return super.isLoaded(this._pageIndicator); }

    //TODO: right now, all input fields are text inputs. probably should change this

    getName() { return this._form.getName(); }
    enterName(name: string) { return this._form.setName(name); }
    clearName() { return this._form.clearName(); }

    getStartDate() { return this._form.getStartDate(); }
    enterStartDate(startDate: Date) { return this._form.setStartDate(startDate); }
    clearStartDate() { return this._form.clearStartDate(); }

    getEndDate() { return this._form.getEndDate(); }
    enterEndDate(endDate: Date) { return this._form.setEndDate(endDate); }
    clearEndDate() { return this._form.clearEndDate(); }

    getAmount() { return this._form.getAmount(); }
    enterAmount(amount: number) { return this._form.setAmount(amount); }
    clearAmount() { return this._form.clearAmount(); }

    getBudget() { return this._form.getBudget(); }
    enterBudget(budget: BudgetData) { return this._form.enterBudget(budget); }

    canSave() { return this._form.canSave(); }
    save() { return this._form.save(); }

    getMsg() { return this._form.getMsg(); }

}
