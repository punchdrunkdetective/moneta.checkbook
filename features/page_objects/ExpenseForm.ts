import { getApp, formatDate } from '../helpers/app';
import { Form } from "./Form";
import { ExpenseData } from "../data_objects/ExpenseData";

const app = getApp();

export class ExpenseFrom extends Form {

    private _entryDateSelector = '//*[@id="entryDate"]';
    private _descriptionSelector = '//*[@id="description"]';
    private _amountSelector = '//*[@id="amount"]';

    getExpense(): Promise<ExpenseData> {
        return Promise.all([
            this.getEntryDate(),
            this.getDescription(),
            this.getAmount()
        ])
        .then((results) => {
            return {
                entryDate: results[0],
                description: results[1],
                amount: results[2]
            };
        });
    }

    setExpense(expense: ExpenseData): Promise<void> { 
        return this.setEntryDate(expense.entryDate)
            .then(() => this.setDescription(expense.description))
            .then(() => this.setAmount(expense.amount));
    }

    clearEntryDate(): Promise<void> { return app.client.setValue(this._entryDateSelector, ' '); } //NOTE: react may not trigger change event with ''
    getEntryDate(): Promise<Date> { return app.client.getValue(this._entryDateSelector).then((dateStr: Date) => dateStr ? new Date(dateStr) : ''); }
    setEntryDate(date: Date): Promise<void> { return app.client.setValue(this._entryDateSelector, formatDate(date)); }    

    clearDescription(): Promise<void> { return app.client.setValue(this._descriptionSelector, ' '); } //NOTE: react may not trigger change event with ''
    getDescription(): Promise<string> { return app.client.getValue(this._descriptionSelector); }
    setDescription(desc: string): Promise<void> { return app.client.setValue(this._descriptionSelector, desc); }    

    clearAmount(): Promise<void> { return app.client.setValue(this._amountSelector, ' '); } //NOTE: react may not trigger change event with ''
    getAmount(): Promise<number> { return app.client.getValue(this._amountSelector).then((amountStr: string) => amountStr ? parseFloat(amountStr) : 0.00); }
    setAmount(amt: number): Promise<void> { return app.client.setValue(this._amountSelector, amt); }
    

}