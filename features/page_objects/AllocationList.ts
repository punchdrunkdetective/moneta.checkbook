import { getApp } from '../helpers/app';
import { AllocationData } from "../data_objects/AllocationData";

const app = getApp();

export class AllocationList {

    //TODO: consider changing input to require just name only
    allocationExists(allocation: AllocationData): Promise<boolean> { return this._waitForExist(allocation, false); }
    allocationNotExists(allocation: AllocationData): Promise<boolean> { return this._waitForExist(allocation, true); }

    getAllocation(allocationName: string): Promise<AllocationData> {
        return this
            ._waitForVisible(allocationName)
            .then(() => {
                let row = this._element(allocationName);
                return Promise.all([
                    row.getText('./td[1]'),
                    row.getText('./td[2]'),
                    row.getText('./td[3]'),
                    row.getText('./td[4]')
                ])
            .then((results) => {
                return {
                    name: results[0],
                    amount: parseFloat(results[1]),
                    spent: parseFloat(results[2]),
                    unspent: parseFloat(results[3])                   
                }
            });
        });
    }

    addAllocation(): Promise<void> { 
        const addSelector = './/*[text()="Add"]';
        return app.client
            .waitForVisible(addSelector)
            .then(() => app.client.click(addSelector)); 
     }

    viewAllocation(allocationName: string): Promise<void> { 
        return this
            ._waitForVisible(allocationName)
            .then(() => this._element(allocationName).click(`.//a[text()="${allocationName}"]`)); 
    }

    updateAllocation(allocationName: string): Promise<void> { 
        return this
            ._waitForVisible(allocationName)
            .then(() => this._element(allocationName).click('.//*[text()="update"]')); 
    }

    deleteAllocation(allocationName: string): Promise<void> { //TODO: maybe make sure this is the delete popup module?
        return this
            ._waitForVisible(allocationName)
            .then(() => this._element(allocationName).click('.//*[text()="delete"]')); 
    }

    confirmDeletion(): Promise<void> { return app.client.alertAccept(); }
    cancelDeletion(): Promise<void> { return app.client.alertDismiss(); }

    getTotalAmount(): Promise<number> { return app.client.getText('//tfoot//td[2]').then((total: string) => parseFloat(total)); } //TODO: consider adding methods for other totals (currently not tested)

    private _rowSelector(allocation: AllocationData): string { return `//*[contains(@class, "allocationList")]//table//tr[td[a[text()="${allocation.name}"]] and td[text()="${allocation.amount.toFixed(2).toString()}"] and td[text()="${(allocation.spent || 0.00).toFixed(2).toString()}"] and td[text()="${(allocation.unspent || 0.00).toFixed(2).toString()}"]]`; }
    private _waitForExist(allocation: AllocationData, reverse: boolean): Promise<boolean> {
        return app.client.waitForExist(this._rowSelector(allocation), 5000, reverse)
            .then(() => true)
            .catch(() => false);
    }

    private _rowByNameSelector(allocationName: string): string { return `//*[contains(@class, "allocationList")]//table//tr[td[a[text()="${allocationName}"]]]`; }
    private _element(allocationName: string): any { return app.client.element(this._rowByNameSelector(allocationName)); }
    private _waitForVisible(allocationName: string): Promise<void> { return app.client.waitForVisible(this._rowByNameSelector(allocationName)); }

}
