@allocationRegression
Feature: Allocate money in a budget.

    As a financial planner, I want to allocate money in a budget for a specific purpose.

    Background:
        Given An existing budget "test budget" of 1200.00
        And The budget "test budget" has an existing 100.00 allocated for "gas"
        And The budget "test budget" has an existing 800.00 allocated for "grocery"
        And The budget "test budget" has an existing 100.00 allocated for "free spending"

    Scenario Outline: Allocate a budget
        Given I am prompted to allocate "test budget"
        When I allocate <amount> for <purpose>
        And I save the new allocation
        Then I should see a success message for allocating a budget
        And The allocation list for "test budget" should show <purpose> <amount> <spent> <unspent>
        And The allocation list for "test budget" should show total amount of <allocated>
        And The budget details for "test budget" should show available funding of <unallocated>

        Examples:
            | purpose | amount | spent | unspent | allocated | unallocated |
            | "food"  | 80.00  | 0.00  | 80.00   | 1080.00   | 120.00      |
            | "waste" | 0.00   | 0.00  | 0.00    | 1000.00   | 200.00      |

    Scenario Outline: Fail to allocate a budget
        Given I am prompted to allocate "test budget"
        When I allocate <amount> for <purpose>
        And I save the new allocation
        Then I should see a failure message for allocating a budget
        And The allocation list for "test budget" should not show <purpose> <amount> <spent> <unspent>
        And The allocation list for "test budget" should show total amount of 1000.00
        And The budget details for "test budget" should show available funding of 200.00

        Examples: 
            | purpose     | amount | spent | unspent |
            | "waste"     | 500.00 | 0.00  | 500.00  |           
            | "damn dept" | -1.00  | 0.00  | -1.00   |
            | "grocery"   | 125.00 | 0.00  | 125.00  |

