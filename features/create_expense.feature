@expenseRegression
Feature: Record an expense against an allocation.

    As a checkbook balancer, I want to record money spent for a specific purpose.

    Background:
        Given An existing budget "test budget" 8/7/2018 9/3/2018 1200.00
        And The budget "test budget" has an existing 1000.00 allocated for "general expenses"
        And The budget "test budget" has an existing 200.00 allocated for "free spending"
        And The allocation "general expenses" in "test budget" has expense 1 8/28/2018 "pre-existing expense" 10.00
        And The allocation details for "general expenses" in "test budget" should show 
            | name    | general expenses |
            | amount  | 1000.00          |
            | spent   | 10.00            |
            | unspent | 990.00           |

    Scenario Outline: Record Expense

        Given I am prompted to enter an expense for "general expenses" in "test budget"
        When I enter <date> <description> <amount> for a new expense
        And I save the new expense
        Then I should see a success message for expense creation
        And The expense list for "general expenses" in "test budget" should show <date> <description> <amount>
        And The expense list for "general expenses" in "test budget" should show a total of <spent>
        And The allocation list for "test budget" should show "general expenses" 1000.00 <spent> <unspent>

        Examples:
            | date      | description                | amount | spent | unspent |
            | 8/7/2018  | "eating out with the boss" | 25.00  | 35.00 | 965.00  |
            | 8/28/2018 | "eating out with the boss" | 25.00  | 35.00 | 965.00  |
            | 9/3/2018  | "eating out with the boss" | 25.00  | 35.00 | 965.00  |
            | 8/28/2018 | ""                         | 25.00  | 35.00 | 965.00  |
            | 8/28/2018 | "pre-existing expense"     | 10.00  | 20.00 | 980.00  |
            | 8/28/2018 | "eating out with the boss" | 0.00   | 10.00 | 990.00  |

    Scenario Outline: Fail to record expense

        Given I am prompted to enter an expense for "general expenses" in "test budget"
        When I enter <date> <description> <amount> for a new expense
        And I save the new expense
        Then I should see a failure message for expense creation
        And The expense list for "general expenses" in "test budget" should not show <date> <description> <amount>
        And The expense list for "general expenses" in "test budget" should show a total of 10.00
        And The allocation list for "test budget" should show "general expenses" 1000.00 10.00 990.00

        Examples:
            | date      | description                | amount | 
            | 8/6/2018  | "eating out with the boss" | 25.00  |
            | 9/4/2018  | "eating out with the boss" | 25.00  |
            | 8/28/2018 | "eating out with the boss" | -25.00 |

