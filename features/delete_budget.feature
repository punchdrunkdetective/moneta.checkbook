@budgetRegression
Feature: Delete a budget.

    As a financial planner, I want to be able to delete a budget that was incorrectly made, or not needed.

    #TODO: Test the scenario for cancelling to delete
    Scenario Outline: Delete From List

        Delete an existing budget from the budget list.

        Given An existing budget <name> <startDate> <endDate> <amount>
        And The budget list contains <name> <startDate> <endDate> <amount>
        When I delete <name> from the budget list
        And I confirm I want to delete the budget
        Then The budget list should not show <name> <startDate> <endDate> <amount>
        And The datastore should not contain <name>

        Examples: 
            | name          | startDate | endDate  | amount |
            | "test budget" | 8/7/2018  | 9/3/2018 | 300.00 |

        
        
