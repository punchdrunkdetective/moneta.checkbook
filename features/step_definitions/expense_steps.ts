import { Given, When, Then } from 'cucumber';
import { ExpenseData } from "../data_objects/ExpenseData";
import { getApp } from '../helpers/app';

import { CreateExpensePage } from '../page_objects/CreateExpensePage';
import { ViewAllocationPage } from '../page_objects/ViewAllocationPage';
import { UpdateExpensePage } from '../page_objects/UpdateExpensePage';

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

const app = getApp(); 
const createPage = new CreateExpensePage();
const viewPage = new ViewAllocationPage();
const updatePage = new UpdateExpensePage();

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

Given('I am prompted to enter an expense for {string} in {string}', (allocationName: string, budgetName: string) =>
    createPage.load(budgetName, allocationName)
);

Given('I am prompted to update expense {int} for {string} in {string}', (expenseId: number, allocationName: string, budgetName: string) =>
    updatePage
        .load(budgetName, allocationName, expenseId)
        .then(() => updatePage.getExpense())
        .then((expense: ExpenseData) => {
            //TODO: the following verifications will be handed by ui integration tests
            //TODO: how to wait for this page to load? currently doesn't seem to be an issue
            //expect(expense.entryDate).to.deep.equal(entryDate, 'unexpected date');
            //expect(expense.description).to.equal(description, 'unexpected description');
            //expect(expense.amount).to.equal(amount, 'unexpected amount');
        })
);

When('I enter {date} {string} {float} for a new expense', (entryDate: Date, description: string, amount: number) =>
    createPage.enterExpense({
        entryDate, 
        description, 
        amount
    })
);

When('I edit expense to {date} {string} {float}', (entryDate: Date, description: string, amount: number) =>
    updatePage.enterExpense({entryDate, description, amount})
);

When('I save the new expense', () =>
    createPage.save()
);

When('I save expense edits', () =>
    updatePage.save()
);

When('I delete {date} {string} {float} from the expense list', (entryDate: Date, description: string, amount: number) =>    
    viewPage.deleteExpense({entryDate, description, amount})
);

When('I confirm I want to delete the expense', () =>
    viewPage.confirmDeletion()
);

When('I cancel I want to delete the expense', () =>
    viewPage.cancelDeletion()
);

Then('I should see a success message for expense creation', () =>
    createPage.getMsg().should.eventually.deep.equal('success')
);

Then('I should see a failure message for expense creation', () =>
    createPage.getMsg().should.eventually.deep.equal('failure')
);

Then('I should see a success message for expense update', () =>
    updatePage.getMsg().should.eventually.deep.equal('success') 
);

Then('I should see a failure message for expense update', () =>
    updatePage.getMsg().should.eventually.deep.equal('failure')
);

const onDetailsPage = (budgetName: string, allocationName: string) => 
    viewPage
        .isLoaded()
        .then((loaded: boolean) => {if(!loaded) return viewPage.load(budgetName, allocationName)});

Then('The expense list for {string} in {string} should show {date} {string} {float}', 
    (allocationName: string, budgetName: string, entryDate: Date, description: string, amount: number) =>
        onDetailsPage(budgetName, allocationName)
            .then(() => viewPage.expenseExists({entryDate, description, amount}).should.eventually.be.true)
    );

Then('The expense list for {string} in {string} should not show {date} {string} {float}', 
    (allocationName: string, budgetName: string, entryDate: Date, description: string, amount: number) =>
        onDetailsPage(budgetName, allocationName)
            .then(() => viewPage.expenseNotExists({entryDate, description, amount}).should.eventually.be.true)
    );

Then('The expense list for {string} in {string} should show a total of {float}', 
    (allocationName: string, budgetName: string, total: number) =>
        onDetailsPage(budgetName, allocationName)
            .then(() => viewPage.getExpenseTotal().should.eventually.equal(total))
    );
