import { Given, When, Then } from 'cucumber';
import { getApp } from '../helpers/app';

import { CreateAllocationPage } from '../page_objects/CreateAllocationPage';
import { ViewBudgetPage } from '../page_objects/ViewBudgetPage';
import { ViewAllocationPage } from '../page_objects/ViewAllocationPage';
import { UpdateAllocationPage } from '../page_objects/UpdateAllocationPage';

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

const app = getApp(); 
const createPage = new CreateAllocationPage();
const viewBudgetPage = new ViewBudgetPage();
const viewAllocationPage = new ViewAllocationPage();
const updatePage = new UpdateAllocationPage();

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

Given('I am prompted to allocate {string}', (budgetName: string) =>        
    createPage.load(budgetName)
);

Given('I am viewing {string} in {string}', (allocationName: string, budgetName: string) =>        
    viewAllocationPage.load(budgetName, allocationName)
);

Given('I am prompted to update {string} in {string}', (allocationName: string, budgetName: string) =>        
    updatePage
        .load(budgetName, allocationName)
        .then(() => updatePage.getName().should.eventually.deep.equal(allocationName, 'unexpeced name'))
);

When('I allocate {float} for {string}', (amount: number, allocationName: string) =>
    createPage.enterAllocation({
        name: allocationName,
        amount: amount
    })
);

When('I edit allocation to {string} {float}', (allocationName: string, amount: number) =>
    updatePage.enterAllocation({
        name: allocationName, 
        amount: amount
    })
);

When('I save the new allocation', () =>
    createPage.save()
);

When('I save the allocation edits', () =>
    updatePage.save()
);

When('I delete {string} from the allocation list', (allocationName: string) =>
    viewBudgetPage.deleteAllocation(allocationName)
);

When('I confirm I want to delete the allocation', () =>
    viewBudgetPage.confirmDeletion() 
);

When('I cancel that I want to delete the allocation', () =>
    viewBudgetPage.cancelDeletion()
);

Then('I (should )see a success message for allocating a budget', () =>
    createPage.getMsg().should.eventually.deep.equal('success')
);

Then('I (should )see a failure message for allocating a budget', () =>
    createPage.getMsg().should.eventually.deep.equal('failure')
);

Then('I (should )see a success message for allocation update', () =>
    updatePage.getMsg().should.eventually.deep.equal('success')
);

Then('I (should )see a failure message for allocation update', () =>
    updatePage.getMsg().should.eventually.deep.equal('failure')
);

Then('The allocation list for {string} should show {string} {float} {float} {float}', 
    (budgetName: string, allocationName: string, amount: number, spent: number, unspent: number) =>
        viewBudgetPage
            .isLoaded()
            .then((loaded: boolean) => {if (!loaded) return viewBudgetPage.load(budgetName)})
            .then(() => viewBudgetPage.allocationExists({
                name: allocationName,
                amount: amount,
                spent: spent,
                unspent: unspent
            }).should.eventually.be.true)
);

Then('The allocation list for {string} should not show {string} {float} {float} {float}', 
    (budgetName: string, allocationName: string, amount: number, spent: number, unspent: number) =>
        viewBudgetPage
            .isLoaded()
            .then((loaded: boolean) => {if (!loaded) return viewBudgetPage.load(budgetName)})
            .then(() => viewBudgetPage.allocationNotExists({
                name: allocationName,
                amount: amount,
                spent: spent,
                unspent: unspent
            }).should.eventually.be.true)
);

Then('The allocation list for {string} should show total amount of {float}',(budgetName: string, totalAmount: number) =>
    viewBudgetPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return viewBudgetPage.load(budgetName)})
        .then(() => viewBudgetPage.getTotalAllocated().should.eventually.equal(totalAmount))
);

Then('The allocation details for {string} in {string} should show', (allocationName: string, budgetName: string, dataTable: any) =>
    viewAllocationPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return viewAllocationPage.load(budgetName, allocationName)})
        .then(() => dataTable.rowsHash())
        .then((table) => viewAllocationPage.getAllocation().should.eventually.deep.equal({
            name: table.name,
            amount: parseFloat(table.amount),
            spent: parseFloat(table.spent),
            unspent: parseFloat(table.unspent)
        }))
);
