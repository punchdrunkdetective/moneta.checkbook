import { Given, When, Then } from 'cucumber';
import { BudgetData } from "../data_objects/BudgetData";
import { getApp } from '../helpers/app';

import { CreateBudgetPage } from '../page_objects/CreateBudgetPage';
import { ViewBudgetPage } from '../page_objects/ViewBudgetPage';
import { UpdateBudgetPage } from '../page_objects/UpdateBudgetPage';
import { IndexPage } from '../page_objects/IndexPage';

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

const app = getApp(); 
const createPage = new CreateBudgetPage();
const viewPage = new ViewBudgetPage();
const updatePage = new UpdateBudgetPage();
const indexPage = new IndexPage();

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

//TODO: assertion errors have very little information
//TODO: consider making class CheckbookDate
//TODO: avialable funding isn't always checked. should it be?

Given('The budget list contains {string} {date} {date} {float}', (name: string, startDate: Date, endDate: Date, amount: number) =>
    app.client
        .pause(1000) //sense index page is the starting point, may need to wait for db setup before force loading the page (fixes selector not found errors)
        .then(() => indexPage.load())
        .then(() => indexPage.budgetExists({
            name: name,
            startDate: startDate,
            endDate: endDate, 
            amount: amount
        }).should.eventually.be.true)
);

Given('I am prompted to create a new budget', () => 
    createPage.load()
);

Given('I am viewing {string}', (budgetName: string) => 
    viewPage.load(budgetName) 
);

Given('I am prompted to update {string}', (budgetName: string) =>
    updatePage
        .load(budgetName)
        .then(() => updatePage.getName().should.eventually.deep.equal(budgetName, 'unexpeced name'))
);

When('I enter {string} {date} {date} {float} for a new budget', (name: string, startDate: Date, endDate: Date, amount: number) =>
    createPage.enterBudget({
        name: name,
        startDate: startDate, 
        endDate: endDate,
        amount: amount
    })
);

When('I edit budget to {string} {date} {date} {float}', (name: string, startDate: Date, endDate: Date, amount: number) =>
    updatePage.enterBudget({ 
        name: name,
        startDate: startDate,
        endDate: endDate,
        amount: amount
    })
);

When('I save the new budget', () => 
    createPage.save()
);

When('I save budget edits', () =>
    updatePage.save()
);

When('I delete {string} from the budget list', (budgetName) => 
    indexPage.deleteBudget(budgetName) 
);

When('I confirm I want to delete the budget', () =>
    indexPage.confirmDeletion()
);

Then('I (should )see a success message for budget creation', () =>
    createPage.getMsg().should.eventually.deep.equal('success')
);

Then('I (should )see a failure message for budget creation', () =>
    createPage.getMsg().should.eventually.deep.equal('failure')
);

Then('I (should )see a success message for budget update', () =>
    updatePage.getMsg().should.eventually.deep.equal('success')
);

Then('I (should )see a failure message for budget update', () =>
    updatePage.getMsg().should.eventually.deep.equal('failure')
);

const detailsShown = (details: BudgetData) => 
    viewPage
        .getBudgeDetails()
        .then((budget: BudgetData) => {
            budget.name.should.equal(details.name, 'unexpeced name');
            budget.startDate.should.deep.equal(details.startDate, 'unexpeced start date');
            budget.endDate.should.deep.equal(details.endDate, 'unexpeced end date');
            budget.amount.should.equal(details.amount, 'unexpeced amount');
        });

const hasAvailableFunding = (amount: number) => 
    viewPage.getAvailableFunding().should.eventually.equal(amount, 'unexpected available funding');

Then('The budget details for {string} should show {string} {date} {date} {float}', (budgetName: string, name: string, startDate: Date, endDate: Date, amount: number) =>
    viewPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return viewPage.load(budgetName)})
        .then(() => detailsShown({
            name: name, 
            startDate: startDate,
            endDate: endDate,
            amount: amount
        }))
);

Then('The budget details for {string} should show available funding of {float}', (budgetName: string, availableFunding: number) =>
    viewPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return viewPage.load(budgetName)})
        .then(() => hasAvailableFunding(availableFunding))
);

const budgetExists = (budget: BudgetData) => 
    indexPage.budgetExists(budget).should.eventually.be.true;

const budgetNotExists = (budget: BudgetData) => 
    indexPage.budgetNotExists(budget).should.eventually.be.true;

Then('The budget list should show {string} {date} {date} {float}', (name: string, startDate: Date, endDate: Date, amount: number) =>
    indexPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return indexPage.load()})
        .then(() => budgetExists({
            name: name,
            startDate: startDate,
            endDate: endDate,
            amount: amount
        }))
);

Then('The budget list should not show {string} {date} {date} {float}', (name: string, startDate: Date, endDate: Date, amount: number) =>
    indexPage
        .isLoaded()
        .then((loaded: boolean) => {if (!loaded) return indexPage.load()})
        .then(() => budgetNotExists({
            name: name,
            startDate: startDate,
            endDate: endDate,
            amount: amount
        }))
);
