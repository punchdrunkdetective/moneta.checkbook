import { Given, Then } from 'cucumber';
import { getApp, getDb } from '../helpers/app';

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

const app = getApp(); 
const db = getDb();

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

Given('An existing budget {string} {date} {date} {float}', 
    (name: string, startDate: Date, endDate: Date, amount: number) =>
        db
        .createBudget({ 
            name: name,
            startDate: startDate, 
            endDate: endDate,
            amount: amount
        })
        .then(() => app.client.waitUntil(
            () => db.budgetExists(name),
            5000,
            "budget could not be setup"))
    );

Given('An existing budget {string} of {float}', 
    (name: string, amount: number) =>
        db
        .createBudget({ 
            name: name,
            startDate: new Date('8/7/2019'), 
            endDate: new Date('9/3/2019'),
            amount: amount
        })
        .then(() => app.client.waitUntil(
            () => db.budgetExists(name),
            5000,
            "budget could not be setup"))
    );

Given('The budget {string} has an existing {float} allocated for {string}', 
    (budgetName: string, amount: number, allocationName: string) =>
        db
        .createAllocation(budgetName, { 
            name: allocationName,
            amount: amount
        })
        .then(() => app.client.waitUntil(
            () => db.allocationExists(budgetName, allocationName),
            5000,
            "allocation could not be setup"))
    );

Given('The allocation {string} in {string} has expense {int} {date} {string} {float}', 
    (allocationName: string, budgetName: string, id: number, entryDate: Date, description: string, amount: number) =>
        db
        .createExpense(budgetName, allocationName, {
            id: id,
            entryDate: entryDate, 
            description: description, 
            amount: amount
        })
        .then(() => app.client.waitUntil(
            () => db.expenseExists(budgetName, allocationName, entryDate, description, amount),
            5000,
            "expense could not be setup"))
    );

Then('The datastore should contain {string}', (budgetName: string) =>
    db
    .reload()
    .then(() => db.budgetExists(budgetName).should.eventually.be.true)
);

Then('The datastore should not contain {string}', (budgetName: string) =>
    db
    .reload()
    .then(() =>db.budgetExists(budgetName).should.eventually.be.false)
);

Then('The datastore should contain {string} in {string}', (allocationName: string, budgetName: string) =>
    db
    .reload()
    .then(() => db.allocationExists(budgetName, allocationName).should.eventually.be.true)
);

Then('The datastore should not contain {string} in {string}', (allocationName: string, budgetName: string) =>
    db
    .reload()
    .then(() => db.allocationExists(budgetName, allocationName).should.eventually.be.false)
);

Then('The datastore should contain {date} {string} {float} for {string} in {string}', 
    (entryDate: Date, description: string, amount: number, allocationName: string, budgetName: string) =>
        db
        .reload()
        .then(() => db.expenseExists(budgetName, allocationName, entryDate, description, amount).should.eventually.be.true)
    );

Then('The datastore should not contain {date} {string} {float} for {string} in {string}', 
    (entryDate: Date, description: string, amount: number, allocationName: string, budgetName: string) =>
        db
        .reload()
        .then(() => db.expenseExists(budgetName, allocationName, entryDate, description, amount).should.eventually.be.false)
    );