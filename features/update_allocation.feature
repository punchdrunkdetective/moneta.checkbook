@allocationRegression
Feature: Update money allocated in a budget.

    As a financial planner, I want to update money allocated in a budget to correct errors or update the planned budget.
    
    Background:
        Given An existing budget "test budget" of 1200.00
        And The budget "test budget" has an existing 100.00 allocated for "gas"
        And The budget "test budget" has an existing 800.00 allocated for "grocery"
        And The budget "test budget" has an existing 100.00 allocated for "free spending"

    Scenario Outline: Update an allocation
        Given I am prompted to update "free spending" in "test budget"
        When I edit allocation to <name> <amount>
        And I save the allocation edits
        Then I should see a success message for allocation update
        And The allocation list for "test budget" should show <name> <amount> <spent> <amount>
        And The allocation list for "test budget" should show total amount of <allocated>
        And The budget details for "test budget" should show available funding of <unallocated>

        Examples:
            | name            | amount  | spent | allocated | unallocated |
            | "misc"          | 100.00  | 0.00  | 1000.00   | 200.00      |
            | "misc"          | 50.00   | 0.00  | 950.00    | 250.00      |
            | "free spending" | 50.00   | 0.00  | 950.00    | 250.00      |
            | "free spending" |  0.00   | 0.00  | 900.00    | 300.00      |            
            | "free spending" | 300.00  | 0.00  | 1200.00   | 0.00        |

    Scenario Outline: Fail to update an allocation
        Given I am prompted to update "free spending" in "test budget"
        When I edit allocation to <name> <amount>
        And I save the allocation edits
        Then I should see a failure message for allocation update
        And The allocation list for "test budget" should not show <name> <amount> 0.00 <amount>
        And The allocation list for "test budget" should show total amount of 1000.00
        And The budget details for "test budget" should show available funding of 200.00

        Examples: 
            | name            | amount | spent |
            | "free spending" | 500.00 | 0.00  |
            | "free spending" | -1.00  | 0.00  |
            | "gas"           | 125.00 | 0.00  |

    Scenario: Update an allocation multiple times

        Update an allocation multiple times in a row, without navigating away from the page.

        Given I am prompted to update "free spending" in "test budget"
        When I edit allocation to "misc" 50.00
        And I save the allocation edits
        And I see a success message for allocation update
        When I edit allocation to "pocket cash" 75.00
        And I save the allocation edits
        Then I should see a success message for allocation update
        And The allocation list for "test budget" should show "pocket cash" 75.00 0.00 75.00
        And The allocation list for "test budget" should show total amount of 975.00
        And The budget details for "test budget" should show available funding of 225.00

    Scenario: Update fully allocated budget
        Given The budget "test budget" has an existing 200.00 allocated for "food"
        And I am prompted to update "food" in "test budget"
        When I edit allocation to "food" 150.00
        And I save the allocation edits
        Then I should see a success message for allocation update
        And The allocation list for "test budget" should show "food" 150.00 0.00 150.00
        And The allocation list for "test budget" should show total amount of 1150.00
        And The budget details for "test budget" should show available funding of 50.00

    Scenario: Fail to update fully allocated budget
        Given The budget "test budget" has an existing 200.00 allocated for "food"
        And I am prompted to update "food" in "test budget"
        When I edit allocation to "food" 250.00
        And I save the allocation edits
        Then I should see a failure message for allocation update
        And The allocation list for "test budget" should not show "food" 250.00 0.00 250.00
        And The allocation list for "test budget" should show total amount of 1200.00
        And The budget details for "test budget" should show available funding of 0.00




