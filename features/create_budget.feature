@budgetRegression
Feature: Create a budget.

    As a financial planner, I want to budget money for a period of time. This budget doesn't 
    need to be further subdivided. Just the ability to say I have X money for Y time.

    Scenario Outline: Correct Input
        
        Add a budget that is unique to the system.

        Given I am prompted to create a new budget
        When I enter "test budget" <startDate> <endDate> <amount> for a new budget
        And I save the new budget
        Then I should see a success message for budget creation
        And The budget list should show "test budget" <startDate> <endDate> <amount>

        Examples: Success
            | startDate  | endDate  | amount |
            | 8/7/2018   | 9/3/2018 | 300.00 |
            | 12/31/2018 | 1/1/2019 | 300.00 |
            | 8/7/2018   | 8/7/2018 | 251.12 |
            | 8/7/2018   | 9/3/2018 | 0.00   | 

    Scenario Outline: Bad Input
        
        Fail to add a budget that is unique to the system, but with bad input.

        Given I am prompted to create a new budget
        When I enter <name> <startDate> <endDate> <amount> for a new budget
        And I save the new budget
        Then I should see a failure message for budget creation
        And The budget list should not show <name> <startDate> <endDate> <amount>

        Examples: 
            | name          | startDate | endDate  | amount  |
            | "test budget" | 9/3/2018  | 8/7/2018 | 300.00  |
            | "test budget" | 8/7/2018  | 8/1/2018 | 300.00  |
            | "test budget" | 8/7/2019  | 9/3/2018 | 300.00  |
            | "test budget" | 8/7/2018  | 9/3/2018 | -300.00 |

    Scenario Outline: Partial Duplicate

        Add a budget that is not fully unique to the system.

        Given An existing budget "test budget" 8/7/2018 9/3/2018 300.00
        And I am prompted to create a new budget
        When I enter <name> <startDate> <endDate> <amount> for a new budget
        And I save the new budget
        Then I should see a <success_or_failure> message for budget creation
        And The budget list <should_or_not> show <name> <startDate> <endDate> <amount>

        Examples: 
            | name            | startDate | endDate   | amount      | success_or_failure | should_or_not  |
            | "test budget"   | 5/10/2018 | 10/5/2018 | 250.50      | failure            | should not     |  
            | "test budget 2" | 8/7/2018  | 10/5/2018 | 250.50      | success            | should         | 
            | "test budget 2" | 5/10/2018 | 9/3/2018  | 250.50      | success            | should         |
            | "test budget 2" | 5/10/2018 | 10/5/2018 | 300.00      | success            | should         |
