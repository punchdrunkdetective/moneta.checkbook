const {app, ipcMain, BrowserWindow, Menu} = require('electron');
const path = require('path');
const url = require('url');

let win; // Binding to global scope prevents garbage collection.

app.on('ready', function () {


    win = new BrowserWindow({
        width: 800,
        height: 600, 
        resizable: false,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    });

    /*
    win.setMenu(Menu.buildFromTemplate([{
        label: app.getName(),
        submenu: [{
            label: 'About',
            click: () => console.log('TODO: write an about statement')
            //accelerator: 'Ctrl+A'
        }, {
            type: 'separator'
        }, {
            label: 'Home',
            click: () => loadUrl('views/index.html')
            //accelerator: 'Ctrl+H'
        },{
            type: 'separator'
        }, {
            label: 'Quit',
            click: () => app.quit()
            //accelerator: 'Ctrl+Q'
        }]
    }]));
    */
    //win.setMenu(null);

    win.on('close', function () {
        win = null; // Allow garbage collection of main window.
    });

    win.on('app-command', function (e, cmd) {
        if (cmd === 'browser-backward' && win.webContents.canGoBack()) {
            win.webContents.goBack();
        }
    })

    loadUrl('views/index.html');

});

app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
      }
});

ipcMain.on('loadUrl', (evt, filePath, query) => loadUrl(filePath, query));
ipcMain.on('logError', (evt, err) => console.log(err)); //TODO: find better way to log errors

function loadUrl(filePath, query) {    
    console.log('loading ... ' + filePath); 
    win.loadURL(url.format({
        pathname: path.join(__dirname, filePath),
        protocol: 'file:',
        slashes: true,
        query: query ? query : {}
    }));
}

