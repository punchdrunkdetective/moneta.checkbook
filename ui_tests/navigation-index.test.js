const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { IndexPage } = require('../features/page_objects/IndexPage'); 
const { CreateBudgetPage } = require('../features/page_objects/CreateBudgetPage');
const { ViewBudgetPage } = require('../features/page_objects/ViewBudgetPage');
const { UpdateBudgetPage } = require('../features/page_objects/UpdateBudgetPage');

const indexPage = new IndexPage();
const createPage = new CreateBudgetPage();
const detailsPage = new ViewBudgetPage();
const updatePage = new UpdateBudgetPage();

describe('Index navigation', function() {
    
    describe('Select add budget', function() {

        before(() => resetData());
        before(() => indexPage.load());
        before(() => indexPage.addBudget());

        it('Should navigate to create budget form', () => createPage.isLoaded().should.eventually.be.true);

    });

    describe('Select budget details', function() {
        
        before(() => resetData());
        before(() => indexPage.load());
        before(() => indexPage.viewBudget('test budget'));

        it('Should navigate to budget details', () => detailsPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select update budget', function() {
        
        before(() => resetData());
        before(() => indexPage.load());
        before(() => indexPage.updateBudget('test budget'));

        it('Should navigate to budget update form', () => updatePage.isLoaded().should.be.eventually.true);

    });
    
});
