const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateExpensePage } = require('../features/page_objects/UpdateExpensePage');
const { ViewAllocationPage } = require('../features/page_objects/ViewAllocationPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const updatePage = new UpdateExpensePage();
const detailsPage = new ViewAllocationPage();
const indexPage = new IndexPage();

describe('Expense update form navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => updatePage.load('test budget', 'Food', 1));
        before(() => updatePage.goHome());

        it('Should navigation home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => updatePage.load('test budget', 'Food', 1));
        before(() => updatePage.goBack());

        it('Should navigate to allocation details', () => detailsPage.isLoaded().should.eventually.be.true);

    });

});
