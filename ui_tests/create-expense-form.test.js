const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateExpensePage } = require('../features/page_objects/CreateExpensePage');
const createPage = new CreateExpensePage();
const testBudgetId = 'test budget';
const testAllocationId = 'Misc';

describe('Expense Create Form', function() {

  describe('Page loading', function() {

    before(() => resetData());
    before(() => createPage.load(testBudgetId, testAllocationId));

    it('Should show empty entry date in form field', () => createPage.getEntryDate().should.eventually.deep.equal(''));
    it('Should show empty description in form field', () => createPage.getDescription().should.eventually.deep.equal(''));
    it('Should show empty amount in form field', () => createPage.getAmount().should.eventually.equal(0.00));
    it('Should have Save disabled by default', () => createPage.canSave().should.eventually.be.false); 

  });

  describe('Entering valid data', function() {

    before(() => resetData());
    before(() => createPage.load(testBudgetId, testAllocationId));
    before(() => createPage.enterExpense({
      entryDate: new Date('1/7/2019'),
      description: 'expense form test',
      amount: 5.01
    }));

    it('Should have Save enabled', () => createPage.canSave().should.eventually.be.true);

  });

  describe('Entering partial data', function() {

    describe('Missing entry date', function() {

      before(() => resetData());
      before(() => createPage.load(testBudgetId, testAllocationId));
      before(() => createPage.enterExpense({
        entryDate: new Date('1/7/2019'),
        description: 'expense form test',
        amount: 5.01
      }));
      before(() => createPage.clearEntryDate());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

    describe('Missing description', function() {

      before(() => resetData());
      before(() => createPage.load(testBudgetId, testAllocationId));
      before(() => createPage.enterExpense({
        entryDate: new Date('1/7/2019'),
        description: 'expense form test',
        amount: 5.01
      }));
      before(() => createPage.clearDescription());

      it('Should have Save enabled', () => createPage.canSave().should.eventually.be.true); 

    });

    describe('Missing amount', function() {

      before(() => resetData());
      before(() => createPage.load(testBudgetId, testAllocationId));
      before(() => createPage.enterExpense({
        entryDate: new Date('1/7/2019'),
        description: 'expense form test',
        amount: 5.01
      }));
      before(() => createPage.clearAmount());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

  });

});



