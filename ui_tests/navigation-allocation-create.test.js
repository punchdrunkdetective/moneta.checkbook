const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateAllocationPage } = require('../features/page_objects/CreateAllocationPage');
const { ViewBudgetPage } = require('../features/page_objects/ViewBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const createPage = new CreateAllocationPage();
const detailsPage = new ViewBudgetPage();
const indexPage = new IndexPage();

describe('Allocation create form navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => createPage.load("test budget"));
        before(() => createPage.goHome())

        it('Should navigation home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => createPage.load("test budget"));
        before(() => createPage.goBack())

        it('Should navigate to budget details', () => detailsPage.isLoaded().should.eventually.be.true);
        
    });

});
