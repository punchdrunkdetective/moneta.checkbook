const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateExpensePage } = require('../features/page_objects/UpdateExpensePage');
const updatePage = new UpdateExpensePage();
const testBudgetid = "test budget";
const testAllocationId = "Misc";
const testExpense = { // taken from test data file
    id: 1,
    entryDate: new Date('1/4/2019'),
    description: 'Amazon Prime movie',
    amount: 4.37
}

describe('Expense Update Form', function() {

    describe('Page loading', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId, testExpense.id));
        
        it('Should load entry date in form field', () => updatePage.getEntryDate().should.eventually.deep.equal(new Date('1/4/2019')));
        it('Should load description in form field', () => updatePage.getDescription().should.eventually.deep.equal('Amazon Prime movie'));
        it('Should load amount in form field', () => updatePage.getAmount().should.eventually.equal(4.37));
        it('Should have Save enabled by default', () => updatePage.canSave().should.eventually.be.true);

    });

    describe('Clearing entry date', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId, testExpense.id));
        before(() => updatePage.clearEntryDate());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

    describe('Clearing description', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId, testExpense.id));
        before(() => updatePage.clearDescription());

        it('Should have Save enabled', () => updatePage.canSave().should.eventually.be.true); 

    });

    describe('Clearing amount', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId, testExpense.id));
        before(() => updatePage.clearAmount());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

});