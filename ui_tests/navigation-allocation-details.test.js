const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { ViewAllocationPage } = require('../features/page_objects/ViewAllocationPage');
const { ViewBudgetPage } = require('../features/page_objects/ViewBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 
const { CreateExpensePage } = require('../features/page_objects/CreateExpensePage');
const { UpdateExpensePage } = require('../features/page_objects/UpdateExpensePage');

const allocationDetailsPage = new ViewAllocationPage();
const budgetDetailsPage = new ViewBudgetPage();
const indexPage = new IndexPage();
const expenseCreatePage = new CreateExpensePage();
const expenseUpdatePage = new UpdateExpensePage();

describe('Allocation details navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => allocationDetailsPage.load('test budget', 'Food'));
        before(() => allocationDetailsPage.goHome());

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => allocationDetailsPage.load('test budget', 'Food'));
        before(() => allocationDetailsPage.goBack());

        it('Should navigate to budget details', () => budgetDetailsPage.isLoaded().should.eventually.be.true);
            
    });
    
    describe('Select add expense', function() {

        before(() => resetData());
        before(() => allocationDetailsPage.load('test budget', 'Food'));
        before(() => allocationDetailsPage.addExpense());

        it('Should navigate to create expense form', () => expenseCreatePage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select update expense', function() {
        
        before(() => resetData());
        before(() => allocationDetailsPage.load('test budget', 'Food'));
        before(() => allocationDetailsPage.updateExpense({ //NOTE: got from test data file
            id: 2,
            entryDate: new Date('1/3/2019'),
            description: 'gas station breakfast',
            amount: 4.39
        }));

        it('Should navigate to update expense form', () => expenseUpdatePage.isLoaded().should.eventually.be.true);

    });

});
