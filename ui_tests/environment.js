const { getApp, getDb } = require('../features/helpers/app');
const app = getApp(); 
const db = getDb();

const _data = require('./test_data');

function clearData() {
    return app.client.pause(1000) 
        .then(() => db.deleteAll());
}

function resetData() {
    return clearData().then(() => db.createBudget(_data[0])); //TODO: find a more generic way, create sequentially 
}

module.exports = {
    getApp, 
    getDb,
    clearData,
    resetData
}