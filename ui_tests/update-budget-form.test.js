const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateBudgetPage } = require('../features/page_objects/UpdateBudgetPage');
const updatePage = new UpdateBudgetPage();
const testBudgetid = "test budget";

describe('Budget Update form', function() {

    describe('Page loading', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid));

        it('Should load name in form field', () => updatePage.getName().should.eventually.deep.equal('test budget'));
        it('Should load start date in form field', () => updatePage.getStartDate().should.eventually.deep.equal(new Date('1/1/2019')));
        it('Should load end date in form field', () => updatePage.getEndDate().should.eventually.deep.equal(new Date('1/8/2019')));
        it('Should load amount in form field', () => updatePage.getAmount().should.eventually.equal(200.00));
        it('Should have Save enabled by default', () => updatePage.canSave().should.eventually.be.true);

    });

    describe('Clearing name', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid));
        before(() => updatePage.clearName());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

    describe('Clearing start date', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid));
        before(() => updatePage.clearStartDate());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

    describe('Clearing end date', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid));
        before(() => updatePage.clearEndDate());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

    describe('Clearing amount', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid));
        before(() => updatePage.clearAmount());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

});