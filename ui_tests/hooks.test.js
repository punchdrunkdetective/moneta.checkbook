const { getApp, getDb } = require('./environment');
const app = getApp();
const db = getDb();

//TODO: rename file, remove test from name

before(function() {
    return db.connect();
});

before(function() {
    return app.start();
});

before(function() {
    app.client.timeouts({ 
        "script": 1000,
        "pageLoad": 7000,
        "implicit": 7500
    });
});

after(function() {
    if (app.isRunning()) {
        return app.stop();
    }
});

after(function() {
    return db.disconnect();
});
