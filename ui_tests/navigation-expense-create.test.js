const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateExpensePage } = require('../features/page_objects/CreateExpensePage')
const { ViewAllocationPage } = require('../features/page_objects/ViewAllocationPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const createPage = new CreateExpensePage();
const detailsPage = new ViewAllocationPage();
const indexPage = new IndexPage();

describe('Expense create form navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => createPage.load('test budget', 'Food'));
        before(() => createPage.goHome());

        it('Should navigation home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => createPage.load('test budget', 'Food'));
        before(() => createPage.goBack());

        it('Should navigate to allocation details', () => detailsPage.isLoaded().should.eventually.be.true);

    });

});
