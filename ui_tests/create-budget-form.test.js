const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateBudgetPage } = require('../features/page_objects/CreateBudgetPage');
const createPage = new CreateBudgetPage();

describe('Budget Create Form', function() {

  describe('Page loading', function() {

    before(() => resetData());
    before(() => createPage.load());

    it('Should show empty name in form field', () => createPage.getName().should.eventually.deep.equal(''));
    it('Should show empty start date in form field', () => createPage.getStartDate().should.eventually.deep.equal(''));
    it('Should show empty end date in form field', () => createPage.getEndDate().should.eventually.deep.equal(''));
    it('Should show empty amount in form field', () => createPage.getAmount().should.eventually.equal(0.00));
    it('Should have Save disabled by default', () => createPage.canSave().should.eventually.be.false); 

  });

  describe('Entering valid data', function() {

    before(() => resetData());
    before(() => createPage.load());
    before(() => createPage.enterBudget({
      name: 'form test',
      startDate: new Date('8/7/1990'),
      endDate: new Date('9/3/1991'),
      amount: 500.01
    }));

    it('Should have Save enabled', () => createPage.canSave().should.eventually.be.true);

  });

  describe('Entering partial data', function() {

    describe('Missing name', function() {

      before(() => resetData());
      before(() => createPage.load());
      before(() => createPage.enterBudget({
        name: 'form test',
        startDate: new Date('8/7/1990'),
        endDate: new Date('9/3/1991'),
        amount: 500.01
      }));
      before(() => createPage.clearName());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

    describe('Missing start date', function() {

      before(() => resetData());
      before(() => createPage.load());
      before(() => createPage.enterBudget({
        name: 'form test',
        startDate: new Date('8/7/1990'),
        endDate: new Date('9/3/1991'),
        amount: 500.01
      }));
      before(() => createPage.clearStartDate());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

    describe('Missing end date', function() {

      before(() => resetData());
      before(() => createPage.load());
      before(() => createPage.enterBudget({
        name: 'form test',
        startDate: new Date('8/7/1990'),
        endDate: new Date('9/3/1991'),
        amount: 500.01
      }));
      before(() => createPage.clearEndDate());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

    describe('Missing amount', function() {

      before(() => resetData());
      before(() => createPage.load());
      before(() => createPage.enterBudget({
        name: 'form test',
        startDate: new Date('8/7/1990'),
        endDate: new Date('9/3/1991'),
        amount: 500.01
      }));
      before(() => createPage.clearAmount());

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

  });

});



