const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { ViewBudgetPage } = require('../features/page_objects/ViewBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 
const { CreateAllocationPage } = require('../features/page_objects/CreateAllocationPage');
const { ViewAllocationPage } = require('../features/page_objects/ViewAllocationPage');
const { UpdateAllocationPage } = require('../features/page_objects/UpdateAllocationPage');

const budgetDetailsPage = new ViewBudgetPage();
const indexPage = new IndexPage();
const allocationCreatePage = new CreateAllocationPage();
const allocationDetailsPage = new ViewAllocationPage();
const allocationUpdatepage = new UpdateAllocationPage();

describe('Budget details navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => budgetDetailsPage.load('test budget'));
        before(() => budgetDetailsPage.goHome());

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => budgetDetailsPage.load('test budget'));
        before(() => budgetDetailsPage.goBack());

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true);
            
    });
    
    describe('Select allocation details', function() {

        before(() => resetData());
        before(() => budgetDetailsPage.load('test budget'));
        before(() => budgetDetailsPage.viewAllocation('Food'));

        it('Should navigate to allocation details', () => allocationDetailsPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select add allocation', function() {

        before(() => resetData());
        before(() => budgetDetailsPage.load('test budget'));
        before(() => budgetDetailsPage.addAllocation());

        it('Should navigate to create allocation form', () => allocationCreatePage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select update allocation', function() {

        before(() => resetData());
        before(() => budgetDetailsPage.load('test budget'));
        before(() => budgetDetailsPage.updateAllocation('Food'));

        it('Should navigate to update allocation form', () => allocationUpdatepage.isLoaded().should.eventually.be.true);

    });
    
});
