const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateAllocationPage } = require('../features/page_objects/CreateAllocationPage');
const createPage = new CreateAllocationPage();
const testBudgetid = "test budget";

describe('Allocation Create Form', function() {

  describe('Page loading', function() {

    before(() => resetData());
    before(() => createPage.load(testBudgetid));

    it('Should show empty name in form field', () => createPage.getName().should.eventually.deep.equal(''));
    it('Should show empty amount in form field', () => createPage.getAmount().should.eventually.equal(0.00));
    it('Should have Save disabled by default', () => createPage.canSave().should.eventually.be.false); 

  });

  describe('Entering valid data', function() {

    before(() => resetData());
    before(() => createPage.load(testBudgetid));
    before(() => createPage.enterAllocation({
      name: 'allocation form test',
      amount: 5.01
    }));

    it('Should have Save enabled', () => createPage.canSave().should.eventually.be.true);

  });

  describe('Entering partial data', function() {

    describe('Missing name', function() {

      before(() => resetData());
      before(() => createPage.load(testBudgetid));
      before(() => createPage.enterAmount(5.01));

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

    describe('Missing amount', function() {

      before(() => resetData());
      before(() => createPage.load(testBudgetid));
      before(() => createPage.enterName('allocation form test'));

      it('Should have Save disabled', () => createPage.canSave().should.eventually.be.false); 

    });

  });

});



