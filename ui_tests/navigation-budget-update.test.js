const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateBudgetPage } = require('../features/page_objects/UpdateBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const updatePage = new UpdateBudgetPage();
const indexPage = new IndexPage();

describe('Budget update form navigation', function() {

    describe('Select home', function() {       

        before(() => resetData());
        before(() => updatePage.load("test budget"));
        before(() => updatePage.goHome());

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => updatePage.load("test budget"));
        before(() => updatePage.goBack())

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true); 

    });

});
