const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { CreateBudgetPage } = require('../features/page_objects/CreateBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const createPage = new CreateBudgetPage();
const indexPage = new IndexPage();

describe('Budget create form navigation', function() {

    describe('Select home', function() {

        before(() => resetData());
        before(() => createPage.load());
        before(() => createPage.goHome());

        it('Should navigate home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => createPage.load());
        before(() => createPage.goBack())

        it('Should navigate to home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
});
