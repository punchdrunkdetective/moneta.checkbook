const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateAllocationPage } = require('../features/page_objects/UpdateAllocationPage');
const { ViewBudgetPage } = require('../features/page_objects/ViewBudgetPage');
const { IndexPage } = require('../features/page_objects/IndexPage'); 

const updatePage = new UpdateAllocationPage();
const detailsPage = new ViewBudgetPage();
const indexPage = new IndexPage();

describe('Allocation update form navigation', function() {

    describe('Select home', function () {

        before(() => resetData());
        before(() => updatePage.load('test budget', 'Food'));
        before(() => updatePage.goHome())

        it('Should navigation home', () => indexPage.isLoaded().should.eventually.be.true);

    });
    
    describe('Select back', function() {

        before(() => resetData());
        before(() => updatePage.load('test budget', 'Food'));
        before(() => updatePage.goBack());

        it('Should navigate to budget details', () => detailsPage.isLoaded().should.eventually.be.true);

    });

});
