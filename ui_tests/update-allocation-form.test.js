const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { getApp, resetData } = require('./environment');;
const app = getApp(); 

chaiAsPromised.transferPromiseness = app.transferPromiseness;
chai.use(chaiAsPromised);
chai.should();

const { UpdateAllocationPage } = require('../features/page_objects/UpdateAllocationPage');
const updatePage = new UpdateAllocationPage();
const testBudgetid = "test budget";
const testAllocationId = "Misc";

describe('Allocation Update form', function() {

    describe('Page loading', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId));

        it('Should load name in form field', () => updatePage.getName().should.eventually.deep.equal('Misc'));
        it('Should load amount in form field', () => updatePage.getAmount().should.eventually.equal(10.00));
        it('Should have Save enabled by default', () => updatePage.canSave().should.eventually.be.true);

    });

    describe('Clearing name', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId));
        before(() => updatePage.clearName());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false); 

    });

    describe('Clearing amount', function() {

        before(() => resetData());
        before(() => updatePage.load(testBudgetid, testAllocationId));
        before(() => updatePage.clearAmount());

        it('Should have Save disabled', () => updatePage.canSave().should.eventually.be.false);

    });

});