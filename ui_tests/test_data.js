module.exports = 
[
    {
        name: 'test budget',
        startDate: new Date('1//1/2019'),
        endDate: new Date('1/8/2019'),
        amount: 200.00,
        allocations: [
            {
                name: 'Food',
                amount: 50.00,
                expenses: [
                    {
                        id: 1,
                        entryDate: new Date('1/3/2019'),
                        description: 'Subway - dinner',
                        amount: 11.68 
                    },
                    {
                        id: 2,
                        entryDate: new Date('1/3/2019'),
                        description: 'gas station breakfast',
                        amount: 4.39
                    },
                    {
                        id: 3,
                        entryDate: new Date('1/5/2019'),
                        description: 'Suttrees - group outing',
                        amount: 29.93
                    }
                ]
            },
            {
                name: 'Grocery',
                amount: 100.00,
                expenses: [
                    {
                        id: 1,
                        entryDate: new Date('1/6/2019'),
                        description: 'cat supplies',
                        amount: 37.48
                    },
                    {
                        id: 2,
                        entryDate: new Date('1/6/2019'),
                        description: 'general groceries',
                        amount: 58.80
                    }
                ]
            },
            {
                name: 'Gas',
                amount: 30.00,
                expenses: []
            },
            {
                name: 'Misc',
                amount: 10.00,
                expenses: [
                    {
                        id: 1,
                        entryDate: new Date('1/4/2019'),
                        description: 'Amazon Prime movie',
                        amount: 4.37
                    }
                ]
            }
        ]
    }
];