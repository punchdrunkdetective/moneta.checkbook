const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// NOTE: alias provides mapping from moneta.juno private vars to expected interface.
const ExpenseSchema = new Schema({
    id: { type: Number, alias: "ID" },
    entryDate: { type: Date, alias: "ENTRY_DATE" },
    description: { type: String, alias: "DESCRIPTION" }, 
    amount: { type: Number, alias: "AMOUNT" }
});

ExpenseSchema.pre("updateOne", function() {
    // Solves problem that updateOne does not translate alias of the update object.
    let update = this.getUpdate();
    let newUpdate = ExpenseDto.translateProps(update);
    this.setUpdate(newUpdate);
});

ExpenseSchema.statics.translateProps = function (e) {
    let _expense = (Object.assign({}, e));        // need to remove readonly getters and rely only on "private" fields
    return ExpenseDto.translateAliases(_expense); // translation resets what it can't translate (ie readonly getters)
}

const ExpenseDto = mongoose.model('Expense', ExpenseSchema);

module.exports = {
    ExpenseSchema,
    ExpenseDto
}