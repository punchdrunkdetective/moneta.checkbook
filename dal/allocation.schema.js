const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { ExpenseSchema, ExpenseDto } = require('./expense.schema');

// NOTE: alias provides mapping from moneta.juno private vars to expected interface.
const AllocationSchema = new Schema({
    name: { type: String, alias: "NAME" }, 
    amount: { type: Number, alias: "AMOUNT" },
    expenses: { type: [ExpenseSchema], alias: "EXPENSES" }
});

AllocationSchema.pre("updateOne", function() {
    // Solves problem that updateOne does not translate alias of the update object.
    let update = this.getUpdate();
    let newUpdate = AllocationDto.translateProps(update);
    this.setUpdate(newUpdate);
});

AllocationSchema.statics.translateProps = function (a) {
    let _allocation = (Object.assign({}, a));                // need to remove readonly getters and rely only on "private" fields
    let props = AllocationDto.translateAliases(_allocation); // translation resets what it can't translate (ie readonly getters)
    props.expenses.forEach(function (e, i) { props.expenses[i] = ExpenseDto.translateProps(e); });
    return AllocationDto.translateAliases(_allocation);      // translation resets what it can't translate (ie readonly getters)
}

const AllocationDto = mongoose.model('Allocation', AllocationSchema);

module.exports = {
    AllocationSchema,
    AllocationDto
}