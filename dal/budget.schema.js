const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { AllocationSchema, AllocationDto } = require('./allocation.schema');

// NOTE: alias provides mapping from moneta.juno private vars to expected interface.
const BudgetSchema = new Schema({
    name: { type: String, unique: true, alias: "NAME" }, 
    amount: { type: Number, alias: "AMOUNT" },
    startDate: { type: Date, alias: "START_DATE" },
    endDate: { type: Date, alias: "END_DATE" },
    allocations: { type: [AllocationSchema], alias: "ALLOCATIONS" }
});

BudgetSchema.pre("updateOne", function() {
    // Solves problem that updateOne does not translate alias of the update object.
    let update = this.getUpdate();
    let newUpdate = BudgetDto.translateProps(update);
    newUpdate.allocations.forEach(function (a, i) { newUpdate.allocations[i] = AllocationDto.translateProps(a); });
    this.setUpdate(newUpdate);
});

BudgetSchema.statics.translateProps = function (b) {
    let _budget = (Object.assign({}, b));       // need to remove readonly getters and rely only on "private" fields
    return BudgetDto.translateAliases(_budget); // translation resets what it can't translate (ie readonly getters)
}

const BudgetDto = mongoose.model('Budget', BudgetSchema);

module.exports = {
    BudgetSchema,
    BudgetDto
}