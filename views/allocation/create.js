const React = require('react');
const ReactDOM = require('react-dom');
const { CreateAllocationForm } = require('../../lib/allocation/create.form.co');

let id = window.page.url.searchParams.get('id');

ReactDOM.render(
    React.createElement(CreateAllocationForm, {id: id}), 
    document.querySelector('#content_node')
);
