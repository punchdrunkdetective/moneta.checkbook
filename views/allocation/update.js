const React = require('react');
const ReactDOM = require('react-dom');
const { UpdateAllocationForm } = require('../../lib/allocation/update.form.co');

let budgetId = window.page.url.searchParams.get('budget');
let allocationId = window.page.url.searchParams.get('allocation');

ReactDOM.render(
    React.createElement(UpdateAllocationForm, {budgetId: budgetId, allocationId: allocationId}), 
    document.querySelector('#content_node')
);