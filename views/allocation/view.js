const React = require('react');
const ReactDOM = require('react-dom');
const { Content } = require('../../lib/allocation/view.co');

const budgetId = window.page.url.searchParams.get('budget');
const allocationId = window.page.url.searchParams.get('allocation');

ReactDOM.render(
    React.createElement(Content, {budgetId, allocationId}), 
    document.querySelector('#content_node')
);

