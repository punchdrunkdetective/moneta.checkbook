const React = require('react');
const ReactDOM = require('react-dom');
const { Content } = require('../lib/index.co');

ReactDOM.render(
    React.createElement(Content), 
    document.querySelector('#content_node')
);
