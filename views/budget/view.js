const React = require('react');
const ReactDOM = require('react-dom');
const { Content } = require('../../lib/budget/view.co');

const budgetId = window.page.url.searchParams.get('id');

ReactDOM.render(
    React.createElement(Content, {budgetId: budgetId}), 
    document.querySelector('#content_node')
);