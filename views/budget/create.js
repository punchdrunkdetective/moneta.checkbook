const React = require('react');
const ReactDOM = require('react-dom');
const { BudgetCreateForm } = require('../../lib/budget/create.form.co');

let id = window.page.url.searchParams.get('id'); //TODO: consider making this a component property

ReactDOM.render(
    React.createElement(BudgetCreateForm, {id: id}), 
    document.querySelector('#content_node')
);
