const React = require('react');
const ReactDOM = require('react-dom');
const { BudgetUpdateForm } = require('../../lib/budget/update.form.co');

let id = window.page.url.searchParams.get('id');

ReactDOM.render(
    React.createElement(BudgetUpdateForm, {id: id}), 
    document.querySelector('#content_node')
);

