const React = require('react');
const ReactDOM = require('react-dom');
const { UpdateExpenseForm } = require('../../lib/expense/update.form.co');

let budgetId = window.page.url.searchParams.get('budget');
let allocationId = window.page.url.searchParams.get('allocation');
let expenseId = parseInt(window.page.url.searchParams.get('expense'));

ReactDOM.render(
    React.createElement(UpdateExpenseForm, {budgetId: budgetId, allocationId: allocationId, expenseId: expenseId}), 
    document.querySelector('#content_node')
);
