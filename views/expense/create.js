const React = require('react');
const ReactDOM = require('react-dom');
const { CreateExpenseForm } = require('../../lib/expense/create.form.co');

let budgetId = window.page.url.searchParams.get('budget');
let allocationId = window.page.url.searchParams.get('allocation');

ReactDOM.render(
    React.createElement(CreateExpenseForm, {budgetId: budgetId, allocationId: allocationId}), 
    document.querySelector('#content_node')
);
