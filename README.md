# Moneta Checkbook 


## What is this repository for? 
moneta.checkbook is a personal project of mine used to explore different technologies and patterns. It is an application designed to model a checkbook, allowing the user to allocate money in a budget and record transactions made within that budget.


## Installation
The application uses mongo db version 4.2.1 x64. 
MongoDB Compass can be used to access the database directly for inspection.


## Contribution guidelines 
TBD


## Who do I talk to? 
Me! at punchdrunkdetective@gmail.com

