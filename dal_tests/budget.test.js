const env = require('./environment');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
chai.should();

const _data = require('./test_data');
const { Budget } = require('moneta.juno');

const Database = require('../database');
const db = new Database();

describe('Creating a budget', function() {

    let testBudget = _data[0];
    let output = false;
    before(() => env.clearDb());

    before(() => db.createBudget(testBudget).then((rtn) => output = rtn)); 

    let result = {};
    before(() => env.getBudget(testBudget.name).then((record) => result = record));
    it('Should have success reported', () => output.should.be.true); 
    it('Should have name given', () => result.name.should.equal(testBudget.name));
    it('Should have amount given', () => result.amount.should.equal(testBudget.amount));

});

// NOTE: No fail case for create budget. No specific domain reason for failure. Always blows up on failure.

describe('Reading a budget', function() {

    let testBudget = new Budget(_data[0]);
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let result = {};
    before(() => db.getBudget('test budget').then((record) => result = record));

    //TODO: test exist function
    it('Should have name', () => result.name.should.equal(testBudget.name));
    it('Should have amount', () => result.amount.should.equal(testBudget.amount));

});

describe('Updating a budget', function() {

    let testBudget = _data[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.updateBudget(testBudget.name, { name: 'updated budget' }).then((rtn) => output = rtn));

    it('Should have success reported', () => output.should.be.true);
    //TODO: verify environment result

});

describe('Updating a nonexistant budget', function() {

    let testBudget = _data[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateBudget("fake budget", { name: 'updated budget' }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Deleting a budget', function() {

    let testBudget = _data[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.deleteBudget(testBudget.name).then((rtn) => output = rtn));

    it('Should have success reported', () => output.should.be.true);
    //TODO: verify environment result

});

describe('Deleting a nonexistant budget', function() {

    let testBudget = _data[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.deleteBudget('fake budget').then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});