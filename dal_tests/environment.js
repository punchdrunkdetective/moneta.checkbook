const { BudgetDto } = require('../dal/budget.schema');

const clearDb = () => BudgetDto.deleteMany();

// NOTE: moneta.juno now compiles to non-enumerable properties, forcing schemas to use alias's. Alais's have some nested path limitations.
const createBudget = (data) => new BudgetDto(data).save(); 

// TODO: Figure out how to use pure Mongo, i.e. query objects such as { "name": budgetName, "allocations.name": allocationName }, "allocations.$"
//       Currently, mongoose alias's do not allow for nested path queries that project only subdocuments.

const getBudget = (name) => BudgetDto.findOne({ name });

const getAllocation = (budgetName, allocationName) => getBudget(budgetName).then((b) => {
    let filter = b.allocations.filter((a) => a.name === allocationName);
    return filter.length > 0 ? filter[0] : null;
});

// TODO: consider making this get by id
const getExpense = (budgetName, allocationName, entryDate, description, amount) => getAllocation(budgetName, allocationName).then((a) => {
    let filter = a.expenses.filter((e) => e.entryDate.getTime() === entryDate.getTime() && e.description === description && e.amount === amount);
    return filter.length > 0 ? filter[0] : null;
});

module.exports = {
    clearDb,
    createBudget,
    getBudget,
    getAllocation,
    getExpense
}