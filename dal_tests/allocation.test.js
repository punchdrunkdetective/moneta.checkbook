const env = require('./environment');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const should = chai.should();

const _data = require('./test_data');
const { Budget } = require('moneta.juno');

const Database = require('../database');
const db = new Database();

describe('Creating a allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = { name: 'test allocation', amount: 10.00 };
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false
    before(() => db.createAllocation(testBudget.name, testAllocation).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getAllocation(testBudget.name, testAllocation.name).then((record) => result = record));

    it('Should have success reported', () => output.should.be.true);
    it('Should have name given', () => result.name.should.equal(testAllocation.name));
    it('Should have amount given', () => result.amount.should.equal(testAllocation.amount));

});

describe('Creating a allocation from a nonexistant budget', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = { name: 'test allocation', amount: 10.00 };
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true
    before(() => db.createAllocation("fake budget", testAllocation).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

//TODO: implement read allocation?

describe('Updating a allocation', function() {

    let newAllocationName = "Updated Allocation";
    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.updateAllocation(testBudget.name, testAllocation.name, { name: newAllocationName }).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getAllocation(testBudget.name, newAllocationName).then((record) => result = record));

    it('Should have success reported', () => output.should.be.true);
    it('Should have updated.', () => result.name.should.equal(newAllocationName));

});

describe('Updating a nonexistant allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateAllocation(testBudget.name, "Fake Allocation", { name: "Updated Allocation" }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Updating a allocation from a nonexistant budget', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateAllocation("Fake Budget", testAllocation.name, { name: "Updated Allocation" }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);
    
});

describe('Deleting a allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.deleteAllocation(testBudget.name, testAllocation.name).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getAllocation(testBudget.name, testAllocation.name).then((record) => result = record));

    it('Should have success reported', () => output.should.be.true);
    it('Should not exist', () => should.not.exist(result));

});

describe('Deleting a nonexistant allocation', function () {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;    
    before(() => db.deleteAllocation(testBudget.name, "fake allocation").then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Deleting a allocation from a nonexistant budget', function () {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget)); 

    let output = true;
    before(() => db.deleteAllocation("fake budget", testAllocation.name).then((rtn) => output = rtn));    

    it('Should have failure reported', () => output.should.be.false);

});