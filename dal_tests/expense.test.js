const env = require('./environment');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const should = chai.should();

const _data = require('./test_data');
const { Budget } = require('moneta.juno');

const Database = require('../database');
const db = new Database();

describe('Creating a expense', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = { entryDate: new Date('1/8/2019'), description: 'test expense', amount: 7.77 };
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false
    before(() => db.createExpense(testBudget.name, testAllocation.name, testExpense).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getExpense(testBudget.name, testAllocation.name, testExpense.entryDate, testExpense.description, testExpense.amount).then((output) => result = output));
    it('Should have success reported', () => output.should.be.true); 
    it('Should have entry date given', () => result.entryDate.should.eql(testExpense.entryDate));
    it('Should have description given', () => result.description.should.equal(testExpense.description));
    it('Should have amount given', () => result.amount.should.equal(testExpense.amount));

});

describe('Creating a expense from a nonexistant allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = { entryDate: new Date('1/8/2019'), description: 'test expense', amount: 7.77 };
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true
    before(() => db.createExpense(testBudget.name, "fake allocation", testExpense).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Creating a expense from a nonexistant budget', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = { entryDate: new Date('1/8/2019'), description: 'test expense', amount: 7.77 };
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true
    before(() => db.createExpense("fake budget", testAllocation.name, testExpense).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

//TODO: implement read expense?

describe('Updating a expense', function() {

    let newExpenseDescription = "updated expense";
    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.updateExpense(testBudget.name, testAllocation.name, testExpense.id, { description: newExpenseDescription }).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getExpense(testBudget.name, testAllocation.name, testExpense.entryDate, newExpenseDescription, testExpense.amount).then((output) => result = output));

    it('Should have success reported', () => output.should.be.true); 
    it('Should have updated', () => result.description.should.equal(newExpenseDescription));

});

describe('Updating a non existant expense', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateExpense(testBudget.name, testAllocation.name, 100, { description: "updated expense" }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Updating a expense from a nonexistant allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateExpense(testBudget.name, "fake allocation", testExpense.id, { description: "updated expense" }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Updating a expense from a nonexistant budget', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.updateExpense("fake budget", testAllocation.name, testExpense.id, { description: "updated expense" }).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Deleting a expense', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = false;
    before(() => db.deleteExpense(testBudget.name, testAllocation.name, testExpense.id).then((rtn) => output = rtn));

    let result = {};
    before(() => env.getExpense(testBudget.name, testAllocation.name, testExpense.entryDate, testExpense.description, testExpense.amount).then((output) => result = output));

    it('Should have success reported', () => output.should.be.true); 
    it('Should not exist', () => should.not.exist(result));

});

describe('Deleting a nonexistant expense', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.deleteExpense(testBudget.name, testAllocation.name, 100).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Deleting a expense from a nonexistant allocation', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.deleteExpense(testBudget.name, "fake allocation", testExpense.id).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});

describe('Deleting a expense from a nonexistant budget', function() {

    let testBudget = new Budget(_data[0]);
    let testAllocation = testBudget.allocations[0];
    let testExpense = testAllocation.expenses[0];
    before(() => env.clearDb()); 
    before(() => env.createBudget(testBudget));

    let output = true;
    before(() => db.deleteExpense("fake budget", testAllocation.name, testExpense.id).then((rtn) => output = rtn));

    it('Should have failure reported', () => output.should.be.false);

});
